package util;

import org.apache.log4j.Logger;


public class IdeatorLogger {
    private static Logger logger = Logger.getRootLogger();

    public static void debug(String message) {
//        if(logger.isTraceEnabled()) {
            System.out.println(message);
//        }
    }

    public static void error(String message) {
        System.err.println(message);
    }
}
