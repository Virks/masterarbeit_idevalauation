package util;

import java.util.ArrayList;
import java.util.List;

public class ListUtil {
    public static List<String> intersection(List<String> list1, List<String> list2) {
        List<String> intersectionList = new ArrayList<>(list2);
        intersectionList.retainAll(list1);
        return intersectionList;
    }
}
