package util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class StopWordsHelper {
    private static List<String> englishStopWords = new ArrayList<>();
    private static List<String> germanStopWords = new ArrayList<>();
    private static StopWordsHelper instance;

    public static StopWordsHelper getInstance () {
        if (instance == null) {
            StopWordsHelper.instance = new StopWordsHelper();
        }
        return StopWordsHelper.instance;
    }

    public StopWordsHelper() {
        readStopWordsAndAddToList("EnglishStopwords", englishStopWords);
        readStopWordsAndAddToList("GermanStopwords", germanStopWords);
    }

    private static void readStopWordsAndAddToList(String fileName, List<String> wordList) {
        File file = new File(fileName);
        try(BufferedReader br = new BufferedReader(new FileReader(file)) ) {
            String st;
            while ((st = br.readLine()) != null) {
                wordList.add(st);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean isStopWord (String word) {
        word = word.toLowerCase();
        return word.equals("") || englishStopWords.contains(word) || germanStopWords.contains(word);
    }

    public static boolean isStopWordOrPlural (String inputText, String word) {
        word = word.toLowerCase();
        inputText = inputText.toLowerCase();
        return word.equals("") || englishStopWords.contains(word) || germanStopWords.contains(word) || (word.startsWith(inputText) && (word.length() - inputText.length()) < 2) || inputText.contains(word);
    }
}

