package util;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;
import java.util.function.Consumer;
import java.util.function.Function;

public class LoggingFuture<T> extends CompletableFuture<T> {
    public LoggingFuture() {
        super();
        this.specifyErrorLog("Unhandled Error in Future");
    }

    private LoggingFuture<T> specifyErrorLog(String errorText) {
        this.exceptionally(error -> {
            IdeatorLogger.error(errorText);
            error.printStackTrace();
            this.completeExceptionally(error);
            return null;
        });
        return this;
    }
}
