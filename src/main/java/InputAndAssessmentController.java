import api.ibmWatson.IbmWatsonAPI;
import api.microsoftSearch.BingSearchAPI;
import api.reddit.RedditSearchAPI;
import api.spellCheck.MicrosoftSpellCheckAPI;
import com.ibm.watson.natural_language_understanding.v1.model.CategoriesResult;
import com.ibm.watson.natural_language_understanding.v1.model.KeywordsResult;
import configuraton.ConfigHandler;
import databaseConnection.DbConnection;
import databaseConnection.DbEntry;
import evaluators.*;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import util.IdeatorLogger;

import java.net.UnknownHostException;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class InputAndAssessmentController {
    public ImageView assessmentImage;
    public TextArea assessmentOutput;
    public Button generateButton;
    public Label generateTopicText;
    public TextArea eGenerateInputTextField;
    public Label eGenerateTopicText;
    @FXML
    Rectangle generateColor;
    @FXML
    Rectangle evaluateColor;
    @FXML
    Rectangle assessmentColor;
    @FXML
    ImageView generateImageView;
    @FXML
    TextArea generateInputTextField;
    @FXML
    ImageView evaluatingImage;

    boolean robotAnimationRunning = false;
    private Timer timer = new Timer();;
//    private static final Image[] LOADINGBOTURLS= {new Image("resources/fxml/images2/solobot00.png"),
//    		new Image("resources/fxml/images2/solobot01.png"),
//    		new Image("resources/fxml/images2/solobot02.png"),
//    		new Image("resources/fxml/images2/solobot03.png")};
    private static final Image[] LOADINGBOTURLS= {new Image(InputAndAssessmentController.class.getResourceAsStream("fxml/images2/solobot00.png")),
          new Image(InputAndAssessmentController.class.getResourceAsStream("fxml/images2/solobot01.png")),
          new Image(InputAndAssessmentController.class.getResourceAsStream("fxml/images2/solobot02.png")),
          new Image(InputAndAssessmentController.class.getResourceAsStream("fxml/images2/solobot03.png"))};
    public void initialize() {
        eGenerateTopicText.setText("Topic:\n" + ConfigHandler.getTopicDetailed());
        generateTopicText.setText("Topic: " + ConfigHandler.getTopicshort());
    }

    public void showGeneratePane(boolean show) {
            generateInputTextField.setVisible(show);
            generateImageView.setVisible(show);
            generateButton.setVisible(show);
            generateTopicText.setVisible(show);
    }

    public void showEvaluationPane(boolean show) {
        if(show) {
            if(!robotAnimationRunning) {
                evaluatingImage.setVisible(true);
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    int showImage = 0;
                    int secondspassed= 0;
                    @Override
                    public void run() {
                        evaluatingImage.setImage(LOADINGBOTURLS[showImage]);
                        showImage++;
                        secondspassed += 500;
                        if (showImage > LOADINGBOTURLS.length - 1) {
                            showImage = 0;
                        }
                    }
                }, 0, 500);
                robotAnimationRunning = true;
            }
        } else {
            if(robotAnimationRunning) {
                timer.cancel();
                timer.purge();
                evaluatingImage.setVisible(false);
                robotAnimationRunning = false;
            }
        }
    }

    public void  showAssessmentPane(boolean show) {
        assessmentImage.setVisible(show);
        assessmentOutput.setVisible(show);
    }

    public void generateClicked(MouseEvent mouseEvent) {
        showGenerationContext();
    }

    private void showContext(boolean showGeneratePane, boolean showEvaluationPane, boolean showAssessmentPane) {
        showEvaluationPane(showEvaluationPane);
        showGeneratePane(showGeneratePane);
        showAssessmentPane(showAssessmentPane);
        eGenerateTopicText.setVisible(false);
        eGenerateInputTextField.setVisible(false);
        generateColor.setFill(showGeneratePane ? Color.rgb(255,212,38) : Color.TRANSPARENT);
        evaluateColor.setFill(showEvaluationPane ? Color.rgb(255,212,38) : Color.TRANSPARENT);
        assessmentColor.setFill(showAssessmentPane ? Color.rgb(255,212,38) : Color.TRANSPARENT);
    }

    private void showGenerationContext() {
        showContext(true, false, false);
    }
    private void showEvaluationContext() {
        showContext(false, true, false);
    }
    private void showAssessmentContext() {
        showContext(false, false, true);
    }

    public void assessmentclicked(MouseEvent mouseEvent) {
        showAssessmentContext();
    }

    public void evaluate(MouseEvent mouseEvent) {
        if(mouseEvent.getButton() == MouseButton.PRIMARY) {
        	if(new StringTokenizer(generateInputTextField.getText()).countTokens() < 10) {
        		generateInputTextField.setText("");
        		generateInputTextField.setPromptText("please input a proper idea with at least 10 words");
        		return;
        	}
            showEvaluationContext();
            evaluateIdea(generateInputTextField.getText(), ConfigHandler.getTopicDetailed(), text -> {
                assessmentOutput.setText(text);
                showAssessmentContext();
            });
        }
    }

    public void evaluateIdea(String inputText, String topicText, Consumer<String> evaluationConsumer) {
        IbmWatsonAPI inputTextCategoriesAndKeywordsIbmWatson = new IbmWatsonAPI().startCategoriesAndKeywords(inputText);
        CompletableFuture<List<KeywordsResult>> inputTextKeywords = inputTextCategoriesAndKeywordsIbmWatson.getKeywordsFuture();
        IbmWatsonAPI topicTextCategoriesAndKeywordsIbmWatson = new IbmWatsonAPI().startCategoriesAndKeywords(topicText);
        CompletableFuture<List<KeywordsResult>> topicTextKeywords = topicTextCategoriesAndKeywordsIbmWatson.getKeywordsFuture();

        BingSearchAPI inputTextBingSearchAPI = new BingSearchAPI().startBingSearch(inputTextKeywords.thenApply(IbmWatsonAPI::transformKeywordsToString));
        BingSearchAPI topicTextBingSearchAPI = new BingSearchAPI().startBingSearch(topicTextKeywords.thenApply(IbmWatsonAPI::transformKeywordsToString));
        BingSearchAPI topicTextWithInputTextSearchAPI = new BingSearchAPI().startBingSearch(mergeTopicAndInputTextKeywords(inputTextKeywords, topicTextKeywords));
        MicrosoftSpellCheckAPI inputTextSpellCheckAPI = new MicrosoftSpellCheckAPI().requestTextAnalysisAPI(inputText);
        RedditSearchAPI inputTextRedditSearch = new RedditSearchAPI().startSearch(inputTextKeywords.thenApply(IbmWatsonAPI::removeEntrysWithEigennamen));
        IbmWatsonAPI keyWordsFromSearches = new IbmWatsonAPI().startKeywordsSearch(inputTextBingSearchAPI.transformSnippetsToString(inputTextBingSearchAPI.getSnippetListFuture(), topicTextBingSearchAPI.getSnippetListFuture(), topicTextWithInputTextSearchAPI.getSnippetListFuture()));
        IbmWatsonAPI sentimentFromComments = new IbmWatsonAPI().startSentimentSearch(inputTextRedditSearch.getSearchFuture().thenApply(inputTextRedditSearch::transformSubmissionCommentsToString));

        RarityEvaluator rarityEvaluator = new RarityEvaluator(topicTextBingSearchAPI, topicTextWithInputTextSearchAPI);
        OriginalityEvaluator originalityEvaluator = new OriginalityEvaluator(inputTextRedditSearch);
        ClarityEvaluator clarityEvaluator = new ClarityEvaluator(inputTextSpellCheckAPI);
        TopicRelationEvaluator topicRelationEvaluatior = new TopicRelationEvaluator(inputTextCategoriesAndKeywordsIbmWatson, topicTextCategoriesAndKeywordsIbmWatson);
        AcceptabilityEvaluator acceptabilityEvaluator = new AcceptabilityEvaluator(sentimentFromComments);
        CompletenessEvaluator completednessEvaluator = new CompletenessEvaluator(keyWordsFromSearches, inputText);

        List<IEvaluator> evaluatorList = addEvaluatorsToList(rarityEvaluator, originalityEvaluator, topicRelationEvaluatior, completednessEvaluator,acceptabilityEvaluator, clarityEvaluator);
        evaluatorList.forEach(IEvaluator::evaluate);

        finishEvaluation(evaluationConsumer, evaluatorList, topicRelationEvaluatior, completednessEvaluator);

        saveEvaluationToDatabase(inputText, topicText, inputTextCategoriesAndKeywordsIbmWatson, topicTextCategoriesAndKeywordsIbmWatson, inputTextBingSearchAPI, topicTextBingSearchAPI ,inputTextSpellCheckAPI,
                inputTextRedditSearch, keyWordsFromSearches, sentimentFromComments, rarityEvaluator, originalityEvaluator, topicRelationEvaluatior, completednessEvaluator,acceptabilityEvaluator, clarityEvaluator, topicTextWithInputTextSearchAPI);
    }

    private List<IEvaluator> addEvaluatorsToList(IEvaluator... evaluators) {
        List<IEvaluator> evaluatorList = new ArrayList<>();
        Collections.addAll(evaluatorList, evaluators);
        return evaluatorList;
    }

    private void saveEvaluationToDatabase(String inputText, String topicText, IbmWatsonAPI inputTextCategoriesAndKeywordsIbmWatson,
                                          IbmWatsonAPI topicTextCategoriesAndKeywordsIbmWatson, BingSearchAPI inputTextBingSearchAPI,
                                          BingSearchAPI topicTextBingSearchAPI, MicrosoftSpellCheckAPI inputTextSpellCheckAPI,
                                          RedditSearchAPI inputTextRedditSearch, IbmWatsonAPI keyWordsFromSearches, IbmWatsonAPI sentimentFromComments,
                                          RarityEvaluator rarityEvaluator, OriginalityEvaluator originalityEvaluator, TopicRelationEvaluator topicRelationEvaluatior,
                                          CompletenessEvaluator completednessEvaluator, AcceptabilityEvaluator acceptabilityEvaluator,
                                          ClarityEvaluator clarityEvaluator, BingSearchAPI topicTextWithInputTextSearchAPI) {
        CompletableFuture.allOf(rarityEvaluator.getScore(), originalityEvaluator.getScore(), topicRelationEvaluatior.getScore(), completednessEvaluator.getScore(), acceptabilityEvaluator.getScore(), clarityEvaluator.getScore())
                .thenAccept(e ->  {
                    try {
                        DbEntry.DbEntryBuilder entryBuilder = new DbEntry.DbEntryBuilder();
                        entryBuilder.setInputText(inputText)
                                .setTopicText(topicText)
                                .setRarityScore(rarityEvaluator.getScore().get())
                                .setOriginalityScore(originalityEvaluator.getScore().get())
                                .setTopicRelationScore(topicRelationEvaluatior.getScore().get())
                                .setClarityScore(clarityEvaluator.getScore().get())
                                .setCompletenessScore(completednessEvaluator.getScore().get())
                                .setSocialAcceptabilityScore(acceptabilityEvaluator.getScore().get())
                                .setInputTextSnippetList(inputTextBingSearchAPI.getSnippetListFuture().get())
                                .setTopicTextSnippetList(topicTextBingSearchAPI.getSnippetListFuture().get())
                                .setMergedTextSnippetList(topicTextWithInputTextSearchAPI.getSnippetListFuture().get())
                                .setInputTextTotalBingSearchMatches(inputTextBingSearchAPI.getTotalEstimatedMatchesFuture().get())
                                .setTopicTextTotalBingSearchMatches(topicTextBingSearchAPI.getTotalEstimatedMatchesFuture().get())
                                .setMergedTextTotalBingSearchMatches(topicTextWithInputTextSearchAPI.getTotalEstimatedMatchesFuture().get())
                                .setInputTextKeyWordList(inputTextCategoriesAndKeywordsIbmWatson.getKeywordsFuture().get())
                                .setInputTextKeywordsAsString(inputTextCategoriesAndKeywordsIbmWatson.getKeywordsFuture().thenApply(IbmWatsonAPI::transformAllKeywordsToString).get())
                                .setTopicTextKeyWordList(topicTextCategoriesAndKeywordsIbmWatson.getKeywordsFuture().get())
                                .setTopicTextKeyWordListAsString(topicTextCategoriesAndKeywordsIbmWatson.getKeywordsFuture().thenApply(IbmWatsonAPI::transformAllKeywordsToString).get())
                                .setTopicTextCategoriesList(topicTextCategoriesAndKeywordsIbmWatson.getCategoriesFuture().get())
                                .setTopicTextCategoriesListAsString(IbmWatsonAPI.transformStringListToString(topicTextCategoriesAndKeywordsIbmWatson.getCategoriesFuture().get().stream().map(CategoriesResult::getLabel).collect(Collectors.toList())))
                                .setInputTextCategoriesList(inputTextCategoriesAndKeywordsIbmWatson.getCategoriesFuture().get())
                                .setInputTextCategoriesListAsString(IbmWatsonAPI.transformStringListToString(inputTextCategoriesAndKeywordsIbmWatson.getCategoriesFuture().get().stream().map(CategoriesResult::getLabel).collect(Collectors.toList())))
                                .setKeywordsFromSearchesString(keyWordsFromSearches.getKeywordsFuture().thenApply(IbmWatsonAPI::transformAllKeywordsToString).get())
                                .setRedditCommentsKeywordList(keyWordsFromSearches.getKeywordsFuture().get())
                                .setRedditCommentSentimentScore(sentimentFromComments.getSentimentFuture().get())
                                .setRedditSubmissionList(inputTextRedditSearch.getSearchFuture().get())
                                .setInputTextSpellCheckResult(inputTextSpellCheckAPI.getResponseFuture().get());
                        new DbConnection().saveEvaluationToDatabase(entryBuilder.build());

                    } catch (InterruptedException | UnknownHostException | ExecutionException ex) {
                        ex.printStackTrace();
                    }

                }).exceptionally(error -> {
            IdeatorLogger.error("error while putting to db");
            error.printStackTrace();
            return null;
        });
    }

    private void finishEvaluation(Consumer<String> evaluationConsumer, List<IEvaluator> evaluators, TopicRelationEvaluator topicRelationEvaluatior, CompletenessEvaluator completednessEvaluator) {
        CompletableFuture.allOf(evaluators.stream().map(IEvaluator::getScore).toArray(CompletableFuture[]::new)).thenAccept(e -> {
            StringBuilder evalTextBuilder = new StringBuilder();
            String topics = "";
            String complete = "";
            AsssessmentFormulator asssessmentFormulator = new AsssessmentFormulator(evaluators);

            evalTextBuilder.append("\n").append("Evaluation: ").append("\n");
            evalTextBuilder.append("\n").append("Total-Score:").append(asssessmentFormulator.getTotalScore());
            evaluators.stream().map(eval -> eval.getScore().thenApply(score -> eval.getScoreName() + "-Score: " + score))
                    .map(CompletableFuture::join)
                    .forEach(scoreString -> {
                        evalTextBuilder.append("\n").append(scoreString);
                    });
            try {
                topics = topicRelationEvaluatior.getMostReleventTopic().get();
                complete = completednessEvaluator.getMostValuedKeyword().get();
            } catch (InterruptedException | ExecutionException ex) {
                ex.printStackTrace();
            }

            evalTextBuilder.append("\n").append(MessageFormat.format(asssessmentFormulator.formulateAssessmentText(), topics, complete));
            evalTextBuilder.append("\n\n");
            evaluationConsumer.accept(evalTextBuilder.toString());
        });
    }

    private CompletableFuture<String> mergeTopicAndInputTextKeywords(CompletableFuture<List<KeywordsResult>> inputTextKeywords, CompletableFuture<List<KeywordsResult>> topicTextKeywords) {
        CompletableFuture<String> mergedText = new CompletableFuture<>();
        inputTextKeywords.thenAccept(inputKeywords -> {
           topicTextKeywords.thenAccept(topicKeywords -> {
               StringBuilder keywordString = new StringBuilder("");
               //TODO check why this can be null
               int counter = 0;
               if(inputKeywords != null && !inputKeywords.isEmpty() && topicKeywords != null && !topicKeywords.isEmpty()) {
                   for(int i = 0; counter < 5 && i < topicKeywords.size(); i++) {
                       if (topicKeywords.get(i) != null && topicKeywords.get(i).getText() != null) {
                           String keyword = topicKeywords.get(i).getText();
                           if(topicKeywords.get(i).getText().contains("StayHome")) {
                           } else if(topicKeywords.get(i).getText().contains("CoroNow")) {
                           } else if(topicKeywords.get(i).getText().contains("JoinIn")) {
                           }else {
                               keywordString.append(keyword).append(" ");
                               counter++;
                           }
                       }
                   }
                   counter = 0;
                   for(int i = 0; counter < 2 && i < inputKeywords.size(); i++) {
                       if (inputKeywords.get(i) != null && inputKeywords.get(i).getText() != null) {
                           String keyword = inputKeywords.get(i).getText();
                           if(inputKeywords.get(i).getText().contains("StayHome")) {
                           } else if(inputKeywords.get(i).getText().contains("CoroNow")) {
                           } else if(inputKeywords.get(i).getText().contains("JoinIn")) {
                           }else {
                               keywordString.append(keyword).append(" ");
                               counter++;
                           }
                       }
                   }
                   mergedText.complete(keywordString.toString());
               }
           });
        });

        return mergedText;
    }

    public void extendInputIdeaText(MouseEvent keyEvent) {
        eGenerateInputTextField.setVisible(false);
        eGenerateTopicText.setVisible(false);
        generateInputTextField.setVisible(true);
        generateTopicText.setVisible(true);
    }

    public void extendTopicText(MouseEvent mouseEvent) {
        generateInputTextField.setVisible(false);
        generateTopicText.setVisible(false);
        eGenerateInputTextField.setVisible(true);
        eGenerateTopicText.setVisible(true);
    }

    public void transferTextToBig(KeyEvent keyEvent) {
        generateInputTextField.setText(eGenerateInputTextField.getText());
    }

    public void transferTextToSmall(KeyEvent keyEvent) {
        eGenerateInputTextField.setText(generateInputTextField.getText());
    }
}
