package configuraton;

import java.io.*;
import java.net.URLDecoder;
import java.util.Properties;

public class ConfigHandler {
    public static final String CUSTOM_SEARCH_APIHOST = "BingCustomSearchAPIhost";
    public static final String CUSTOM_SEARCH_APIPATH = "BingCustomSearchAPIpath";
    public static final String BING_CUSTOM_SEARCH_APISUBSCRIPTION_KEY = "BingCustomSearchAPIsubscriptionKey";
    public static final String TWITTER_MAX_SEARCH_RESULTS = "TwitterMaxSearchResults";
    public static final String TWITTER_OAUTH_CONSUMER_KEY = "TwitterOAuthConsumerKey";
    public static final String TWITTER_OAUTH_CONSUMER_SECRET = "TwitterOAuthConsumerSecret";
    public static final String TWITTER_OAUTH_ACCESS_TOKEN = "TwitterOAuthAccessToken";
    public static final String MICROSOFT_TEXT_ANALYSIS_API_HOST = "MicrosoftTextAnalysisAPIHost";
    public static final String MICROSOFT_TEXT_ANALYSIS_API_PATH = "MicrosoftTextAnalysisAPIPath";
    public static final String MICROSOFT_TEXT_ANALYSIS_API_SENTIENT_PATH = "MicrosoftTextAnalysisAPISentientPath";
    public static final String MICROSOFT_TEXT_ANALYSIS_API_KEY_PHRASES_PATH = "MicrosoftTextAnalysisAPIKeyPhrasesPath";
    public static final String MICROSOFT_TEXT_ANALYSIS_API_SUBSCRIPTION_KEY = "MicrosoftTextAnalysisAPISubscriptionKey";
    public static final String MICROSOFT_SPELL_CHECK_API_HOST = "MicrosoftSpellCheckAPIHost";
    public static final String MICROSOFT_SPELL_CHECK_API_PATH = "MicrosoftSpellCheckAPIPath";
    public static final String MICROSOFT_SPELL_CHECK_API_SUBSCRIPTION_KEY = "MicrosoftSpellCheckAPISubscriptionKey";
    public static final String MICROSOFT_SPELL_CHECK_API_LANGUAGE = "MicrosoftSpellCheckAPILanguage";
    public static final String MICROSOFT_SPELL_CHECK_API_MODE = "MicrosoftSpellCheckAPIMode";
    public static final String TWITTER_O_AUTH_ACCESS_TOKEN_SECRET = "TwitterOAuthAccessTokenSecret";
    public static final String CONFIG_PROPERTIES_FILENAME = "config.properties";
    public static final String TOPICDETAILED = "TopicDetailed";
    public static final String TOPICSHORT = "TopicShort";
    public static final String IBMAUTHKEY = "IBMAuthKey";

    private static Properties props;

    private ConfigHandler() {}

    public static void loadConfig() {
        try {
        	File configFile = new File(CONFIG_PROPERTIES_FILENAME);
            FileReader reader = new FileReader(configFile);
            props = new Properties();
            // load the properties file:
            props.load(reader);
        } catch(IOException e) {
        }
    }

    public static Properties getProperties(){return props;};
    public static String getWindowWidth() {
        return props.getProperty("WindowWidth");
    }
    public static String getWindowHeight() {
        return props.getProperty("WindowHeight");
    }
    public static String getBingCustomSearchAPIhost() {
        return props.getProperty(CUSTOM_SEARCH_APIHOST);
    }
    public static String getBingCustomSearchAPIpath() {
        return props.getProperty(CUSTOM_SEARCH_APIPATH);
    }
    public static String getBingCustomSearchAPIsubscriptionKey() {
        return props.getProperty(BING_CUSTOM_SEARCH_APISUBSCRIPTION_KEY);
    }
    public static int getTwitterMaxSearchResults() {
        return Integer.parseInt(props.getProperty(TWITTER_MAX_SEARCH_RESULTS));
    }
    public static String getTwitterOAuthConsumerKey() {
        return props.getProperty(TWITTER_OAUTH_CONSUMER_KEY);
    }
    public static String getTwitterOAuthConsumerSecret() {
        return props.getProperty(TWITTER_OAUTH_CONSUMER_SECRET);
    }
    public static String getTwitterOAuthAccessToken() {
        return props.getProperty(TWITTER_OAUTH_ACCESS_TOKEN);
    }
    public static String getTwitterOAuthAccessTokenSecret() {
        return props.getProperty(TWITTER_O_AUTH_ACCESS_TOKEN_SECRET);
    }
    public static String getMicrosoftTextAnalysisAPIHost() {
        return props.getProperty(MICROSOFT_TEXT_ANALYSIS_API_HOST);
    }
    public static String getMicrosoftTextAnalysisAPIPath() {
        return props.getProperty(MICROSOFT_TEXT_ANALYSIS_API_PATH);
    }
    public static String getMicrosoftTextAnalysisAPISentientPath() {
        return props.getProperty(MICROSOFT_TEXT_ANALYSIS_API_SENTIENT_PATH);
    }
    public static String getMicrosoftTextAnalysisAPIKeyPhrasesPath() {
        return props.getProperty(MICROSOFT_TEXT_ANALYSIS_API_KEY_PHRASES_PATH);
    }
    public static String getMicrosoftTextAnalysisAPISubscriptionKey() {
        return props.getProperty(MICROSOFT_TEXT_ANALYSIS_API_SUBSCRIPTION_KEY);
    }
    public static String getMicrosoftSpellCheckAPIHost() {
        return props.getProperty(MICROSOFT_SPELL_CHECK_API_HOST);
    }
    public static String getMicrosoftSpellCheckAPIPath() {
        return props.getProperty(MICROSOFT_SPELL_CHECK_API_PATH);
    }
    public static String getMicrosoftSpellCheckAPISubscriptionKey() {
        return props.getProperty(MICROSOFT_SPELL_CHECK_API_SUBSCRIPTION_KEY);
    }
    public static String getMicrosoftSpellCheckAPILanguage() {
        return props.getProperty(MICROSOFT_SPELL_CHECK_API_LANGUAGE);
    }
    public static String getMicrosoftSpellCheckAPIMode() {
        return props.getProperty(MICROSOFT_SPELL_CHECK_API_MODE);
    }
    public static String getTopicDetailed() {
        return props.getProperty(TOPICDETAILED);
    }

    public static String getTopicshort() {
        return props.getProperty(TOPICSHORT);
    }

    public static String getIBMAuthKey() { return props.getProperty(IBMAUTHKEY);}

    public static void storeProperties() {
        try {
            props.store(new FileOutputStream(CONFIG_PROPERTIES_FILENAME), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
