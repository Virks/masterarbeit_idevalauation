import java.net.URL;

import configuraton.ConfigHandler;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
//    	System.out.println(System.getProperty("user.dir"));
//    	System.out.println(Main.class.getResource("/fxml/adminPanel.fxml"));
//        Parent root = FXMLLoader.load(new URL("jar:file:" + URLDecoder.decode(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString(), "UTF-8") + "!/resources/fxml/UserOverview.fxml"));
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/UserOverview.fxml"));
        primaryStage.setTitle("Ideator");
        primaryStage.setScene(new Scene(root));
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                System.exit(0);
            }
        });
        primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("ApplicationIcon.png")));
//        primaryStage.getIcons().add(new Image("resources/ApplicationIcon.png"));
        primaryStage.show();
    }

    public void startOld(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/evluation.fxml"));
        primaryStage.setTitle("Ideator");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public static void main(String[] args) {
        java.util.logging.Logger.getLogger("org.apache.http.wire").setLevel(java.util.logging.Level.FINEST);
        java.util.logging.Logger.getLogger("org.apache.http.headers").setLevel(java.util.logging.Level.FINEST);
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
        System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
        System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "ERROR");
        System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http", "ERROR");
        System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", "ERROR");
        ConfigHandler.loadConfig();
        launch(args);
    }
}
