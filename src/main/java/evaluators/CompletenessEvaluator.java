package evaluators;

import api.ibmWatson.IbmWatsonAPI;
import com.ibm.watson.natural_language_understanding.v1.model.KeywordsResult;
import opennlp.tools.stemmer.PorterStemmer;
import util.IdeatorLogger;
import util.ListUtil;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class CompletenessEvaluator implements IEvaluator {

    private CompletableFuture<Double> completenessScoreFuture = new CompletableFuture<>();
    private IbmWatsonAPI ibmWatsonAPI;
    private final String ideaInputText;

    public CompletenessEvaluator(IbmWatsonAPI ibmWatsonAPI, String ideaInputText) {
        this.ideaInputText = ideaInputText;
        this.ibmWatsonAPI = ibmWatsonAPI;
    }

    @Override
    public IEvaluator evaluate() {
        IdeatorLogger.debug("evaluate " + getScoreName());
        evaluateCompletenessScore();
        completenessScoreFuture.thenAccept( score -> {
            IdeatorLogger.debug(getScoreName() + "-Evaluator finished: " + score);
        });
        return this;
    }

    @Override
    public CompletableFuture<String> getAssessment() {
        return getNormalizedScore().thenApply(score -> {
            switch (score) {
                case 1:
                    return "Various aspects are covered very well in their breadth and depth. At most, one of the following terms could be checked more precisely: \"{1}\".";
                case 2:
                    return "Various aspects are well covered in their breadth and depth. I still have a few terms that were not considered: \"{1}\".";
                default:
                    return "Many subcomponents were not considered. I would recommend looking further into the following terms and topics: \"{1}\".";
            }
        });
    }

    @Override
    public CompletableFuture<Double> getScore() {
        return completenessScoreFuture;
    }

    @Override
    public CompletableFuture<Integer> getNormalizedScore() {
        return completenessScoreFuture.thenApply( score -> {
            if(score <= 0.1) {
                return 3;
            } else if (score > 0.1 && score <= 0.2) {
                return 2;
            } else {
                return 1;
            }
        });
    }

    @Override
    public String getScoreName() {
        return "Completeness";
    }

    public CompletableFuture<String> getMostValuedKeyword() {
        return ibmWatsonAPI.getKeywordsFuture().thenApply(keywordsResults -> {
            PorterStemmer stemmer = new PorterStemmer();
            List<String> requestTokenList
                    = Collections.list(
                    new StringTokenizer(ideaInputText))
                    .stream()
                    .map(token -> (String) token)
                    .map(stemmer::stem)
                    .collect(Collectors.toList());
            return keywordsResults.stream().map(KeywordsResult::getText)
                    .filter(k -> requestTokenList.stream().anyMatch(k::contains)).limit(3).collect(Collectors.joining(", "));
        });
    }

    private void evaluateCompletenessScore() {
        PorterStemmer stemmer = new PorterStemmer();
        this.ibmWatsonAPI.getKeywordsFuture().thenApply(keywordList ->
                keywordList.stream()
                        .map(KeywordsResult::getText)
                        .map(this::transformKeywordsIntoSingleWords)
                        .flatMap(Collection::stream)
                        .map(stemmer::stem)
                        .       collect(Collectors.toList())
        ).thenAccept(stemmedKeywordList -> {
            List<String> requestTokenList
                    = Collections.list(
                    new StringTokenizer(ideaInputText))
                    .stream()
                    .map(token -> (String) token)
                    .map(stemmer::stem)
                    .collect(Collectors.toList());
            List<String> keywordIntersection =
                    ListUtil.intersection(requestTokenList, stemmedKeywordList);
            completenessScoreFuture.complete(
                    (double)keywordIntersection.size()
                            /stemmedKeywordList.size());
        });
    }

    private List<String>  transformKeywordsIntoSingleWords(String s) {
        return Arrays.stream(s.split(" ")).collect(Collectors.toList());
    }
}
