package evaluators;

import api.ibmWatson.IbmWatsonAPI;
import util.IdeatorLogger;

import java.util.concurrent.CompletableFuture;

public class AcceptabilityEvaluator implements IEvaluator {
    private CompletableFuture<Double> aktzeptiertheitsFuture = new CompletableFuture<>();
    IbmWatsonAPI ibmWatsonAPI;

    public AcceptabilityEvaluator(IbmWatsonAPI ibmWatsonAPI) {
        this.ibmWatsonAPI = ibmWatsonAPI;
    }

    @Override
    public IEvaluator evaluate() {
        IdeatorLogger.debug("evaluate " + getScoreName());
        evaluateAktzeptiertheitsScore();
        aktzeptiertheitsFuture.thenAccept( score -> {
            IdeatorLogger.debug(getScoreName() + "-Evaluator finished: " + score);
        });
        return this;
    }

    @Override
    public CompletableFuture<String> getAssessment() {
        return this.getNormalizedScore().thenApply( score -> {
            switch (score) {
                case 1:
                    return "Moreover, the idea will be very well received by society.";
                case 2:
                    return "Moreover, the idea will be widely accepted by society.";
                default:
                    return "Moreover, the idea will not be very well received by society.";
            }
        });
    }

    @Override
    public CompletableFuture<Double> getScore() {
        return aktzeptiertheitsFuture;
    }

    private void evaluateAktzeptiertheitsScore() {
        ibmWatsonAPI.getSentimentFuture()
                .thenApply(result -> result.getDocument().getScore())
                .thenApply(aktzeptiertheitsFuture::complete);
    }

    @Override
    public CompletableFuture<Integer> getNormalizedScore() {
        return aktzeptiertheitsFuture.thenApply( score -> {
            if(score <= -0.2) {
                return 3;
            } else if (score > -0.2 && score <= 0.2) {
                return 2;
            } else {
                return 1;
            }
        });
    }

    @Override
    public String getScoreName() {
        return "Acceptability";
    }
}
