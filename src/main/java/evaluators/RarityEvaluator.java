package evaluators;

import api.microsoftSearch.BingSearchAPI;
import util.IdeatorLogger;
import util.LoggingFuture;

import java.util.concurrent.CompletableFuture;

public class RarityEvaluator implements IEvaluator {

    private final BingSearchAPI topicTextBingSearchAPI;
    private BingSearchAPI inputTextBingSearchAPI;
    private CompletableFuture<Double> seltenheitsScoreFuture = new CompletableFuture<>();

    public RarityEvaluator(BingSearchAPI topicTextBingSearchAPI, BingSearchAPI inputTextBingSearchAPI) {
        this.topicTextBingSearchAPI = topicTextBingSearchAPI;
        this.inputTextBingSearchAPI = inputTextBingSearchAPI;
    }

    public CompletableFuture<Double> getScore() {
        return seltenheitsScoreFuture;
    }

    @Override
    public CompletableFuture<Integer> getNormalizedScore() {
        return seltenheitsScoreFuture.thenApply( score -> {
            if(score <= 0.1) {
                return 1;
            } else if (score > 0.1 && score <= 0.15) {
                return 2;
            } else {
                return 3;
            }
        });
    }

    @Override
    public String getScoreName() {
        return "Rarity";
    }

    private void evaluateSeltenheitsScore() {
        topicTextBingSearchAPI.getTotalEstimatedMatchesFuture()
                .thenApply(Double::valueOf)
                .thenAccept(topicMatches ->
                        inputTextBingSearchAPI.getTotalEstimatedMatchesFuture()
                                .thenApply(Double::valueOf)
                                .thenAccept(inputMatches ->
                                        seltenheitsScoreFuture.complete(
                                                inputMatches / topicMatches)
                                )
                );
    }
    @Override
    public IEvaluator evaluate() {
        IdeatorLogger.debug("evaluate " + getScoreName());
        evaluateSeltenheitsScore();
        seltenheitsScoreFuture.thenAccept( score -> {
            IdeatorLogger.debug(getScoreName() + "-Evaluator finished: " + score);
        });
        return this;
    }

    @Override
    public CompletableFuture<String> getAssessment() {
        return getNormalizedScore().thenApply(score -> {
            switch (score) {
                case 1:
                    return "The idea is very unique. I haven''t seen many similar ideas so far.";
                case 2:
                    return "The idea has many approaches that I have rarely found in this form.";
                default:
                    return "I have often found similar ideas.";
            }
        });
    }
}
