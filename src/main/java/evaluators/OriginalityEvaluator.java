package evaluators;

import api.reddit.RedditSearchAPI;
import api.reddit.redditData.RedditEntry;
import api.reddit.redditData.RedditSubmission;
import util.IdeatorLogger;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.IntStream;

public class OriginalityEvaluator implements IEvaluator{
    private final RedditSearchAPI redditSearchAPI;
    CompletableFuture<Double> originaltyScoreFuture = new CompletableFuture<>();

    public OriginalityEvaluator(RedditSearchAPI redditSearchAPI) {
        this.redditSearchAPI = redditSearchAPI;
    }

    @Override
    public IEvaluator evaluate() {
        IdeatorLogger.debug("evaluate " + getScoreName());
        evaluateOriginalitaetsScore();
        originaltyScoreFuture.thenAccept( score -> {
            IdeatorLogger.debug(getScoreName() + "-Evaluator finished: " + score);
        });
        return this;
    }

    @Override
    public CompletableFuture<String> getAssessment() {
        return getNormalizedScore().thenApply(score -> {
            switch (score) {
                case 1:
                    return "The concept is brilliant and has a certain element of surprise.";
                case 2:
                    return "The concept has potential and could convince.";
                default:
                    return "The concept does not have that certain something and does not appear particularly original.";
            }
        });
    }

    private void evaluateOriginalitaetsScore() {
        redditSearchAPI.getSearchFuture()
                .thenApply(this::getAverageCommentTimeToSubmission)
                .thenAccept(originaltyScoreFuture::complete);
    }

    @Override
    public CompletableFuture<Double> getScore() {
        return originaltyScoreFuture;
    }

    @Override
    public CompletableFuture<Integer> getNormalizedScore() {
        return originaltyScoreFuture.thenApply( score -> {
            if(score <= 500) {
                return 1;
            } else if (score > 500 && score <= 2000) {
                return 2;
            } else {
                return 3;
            }
        });
    }

    @Override
    public String getScoreName() {
        return "Originality";
    }

    private double getAverageCommentTimeToSubmission(
            List<RedditSubmission> redditSubmissions) {
        return redditSubmissions.stream()
                .mapToDouble(submission ->
                        IntStream.range(0, submission.getComments().size() - 1)
                                .mapToDouble(i -> {
                                    if(i == 0) {
                                        return calcTimeDifference(
                                                submission.getComments().get(i)
                                                ,submission);
                                    } else {
                                        return calcTimeDifference(
                                                submission.getComments().get(i)
                                                ,submission.getComments().get(i-1));
                                    }
                                })
                                .average()
                                .orElse(Double.NaN)
                )
                .filter(difference -> !Double.isNaN(difference))
                .average()
                .orElse(Double.NaN);
    }

    private double calcTimeDifference(RedditEntry comment1, RedditEntry comment2) {
        return ((comment1.getRetrievedOnDate().getTime() - comment2.getRetrievedOnDate().getTime())
                / ((double) 1000 * 60) );
    }
}
