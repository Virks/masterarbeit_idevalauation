package evaluators;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AsssessmentFormulator {
    private List<IEvaluator> evaluators;

    public AsssessmentFormulator(List<IEvaluator> evaluators) {
        this.evaluators = evaluators;
    }

    public String formulateAssessmentText() {
        int totalAssessment = (int) Math.round(evaluators.stream().map(IEvaluator::getNormalizedScore).mapToInt(CompletableFuture::join).average().orElse(1.0));
        String combined = evaluators.stream().map(IEvaluator::getAssessment).map(CompletableFuture::join).collect(Collectors.joining("\n"));
        return getTotalAssessment(totalAssessment) + "\n" + combined;
    }

    public Double getTotalScore() {
        return evaluators.stream().map(IEvaluator::getNormalizedScore).mapToDouble(CompletableFuture::join).average().orElse(1.0);
    }

    private String getTotalAssessment(int totalScore) {
        switch (totalScore) {
            case 1:
                return "I like your idea very much.";
            case 2:
                return "I like your idea.";
            default:
                return "I don''t like your idea.";
        }
    }
}
