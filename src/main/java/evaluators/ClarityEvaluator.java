package evaluators;

import api.spellCheck.MicrosoftSpellCheckAPI;
import util.IdeatorLogger;

import java.util.StringTokenizer;
import java.util.concurrent.CompletableFuture;

public class ClarityEvaluator implements IEvaluator{
    private final MicrosoftSpellCheckAPI spellCheckAPI;
    private CompletableFuture<Double> klarheitsScoreFuture = new CompletableFuture<>();

    public ClarityEvaluator(MicrosoftSpellCheckAPI spellCheckAPI) {
        this.spellCheckAPI = spellCheckAPI;
    }

    private void evaluateKlarheitsScore() {
        spellCheckAPI.getResponseFuture().thenAccept(flaggedTokens -> {
            int wordCount = new StringTokenizer(spellCheckAPI.getInputText())
                    .countTokens();
            int mistakes = flaggedTokens.size();
            klarheitsScoreFuture.complete((double) mistakes/wordCount);
        });
    }
    @Override
    public IEvaluator evaluate() {
        IdeatorLogger.debug("evaluate " + getScoreName());
        evaluateKlarheitsScore();
        klarheitsScoreFuture.thenAccept( score -> {
            IdeatorLogger.debug(getScoreName() + "-Evaluator finished: " + score);
        });
        return this;
    }

    @Override
    public CompletableFuture<String> getAssessment() {
        return this.getNormalizedScore().thenApply( score -> {
            switch (score) {
                case 1:
                    return "Finally, I really liked the wording.";
                case 2:
                    return "Finally, I find the wording appropriate.";
                default:
                    return "Finally, I find the wording not appropriate.";
            }
        });
    }

    @Override
    public CompletableFuture<Double> getScore() {
        return this.klarheitsScoreFuture;
    }

    @Override
    public CompletableFuture<Integer> getNormalizedScore() {
        return klarheitsScoreFuture.thenApply( score -> {
            if(score <= 0.02) {
                return 1;
            } else if (score > 0.02 && score <= 0.04) {
                return 2;
            } else {
                return 3;
            }
        });
    }

    @Override
    public String getScoreName() {
        return "Clarity";
    }
}
