package evaluators;

import util.LoggingFuture;

import java.util.concurrent.CompletableFuture;

public interface IEvaluator {
    IEvaluator evaluate();
    public CompletableFuture<String> getAssessment();
    public CompletableFuture<Double> getScore();
    public CompletableFuture<Integer> getNormalizedScore();
    public String getScoreName();
}
