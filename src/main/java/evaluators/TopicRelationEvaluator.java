package evaluators;

import api.ibmWatson.IbmWatsonAPI;
import com.ibm.watson.natural_language_understanding.v1.model.CategoriesResult;
import util.IdeatorLogger;
import util.ListUtil;
import util.LoggingFuture;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class TopicRelationEvaluator implements IEvaluator {
    private IbmWatsonAPI inputTextCategoriesIbmWatson;
    private IbmWatsonAPI topicTextCategoriesIbmWatson;
    private LoggingFuture<Double> topicRelationScore = new LoggingFuture<>();
    private List<String> intersectionList;

    public TopicRelationEvaluator(IbmWatsonAPI inputTextCategoriesIbmWatson, IbmWatsonAPI topicTextCategoriesIbmWatson) {
        this.inputTextCategoriesIbmWatson = inputTextCategoriesIbmWatson;
        this.topicTextCategoriesIbmWatson = topicTextCategoriesIbmWatson;
    }

    public CompletableFuture<String> getMostReleventTopic() {
        return topicTextCategoriesIbmWatson.getCategoriesFuture()
                .thenApply(cat -> cat.stream()
                        .map(CategoriesResult::getLabel)
                        .map(this::keepTwoSubcategories)
                        .collect(Collectors.toList()))
                .thenApply( cat -> {
                    List<String> icat = new ArrayList<>(cat);
                    icat.retainAll(intersectionList);
                    return cat.stream().findFirst().orElse(icat.stream().findFirst().orElse(" "));
                });
    }

    @Override
    public IEvaluator evaluate() {
        IdeatorLogger.debug("evaluate " + getScoreName());
        evaluateTopicRelationScore();
        topicRelationScore.thenAccept(score -> {
            IdeatorLogger.debug(getScoreName() + "-Evaluator finished: " + score);
        });
        return this;
    }

    @Override
    public CompletableFuture<String> getAssessment() {
        return getNormalizedScore().thenApply(score -> {
            switch (score) {
                case 1:
                    return "This idea completely covers the given topic. At most, \"{0}\" could be looked at in more detail.";
                case 2:
                    return "This idea deals with some topic elements. However, I would recommend looking deeper into \"{0}\".";
                default:
                    return "The idea does not refer to the given topic. The topic \"{0}\" should also be considered.";
            }
        });
    }

    @Override
    public CompletableFuture<Double> getScore() {
        return topicRelationScore;
    }

    @Override
    public CompletableFuture<Integer> getNormalizedScore() {
        return topicRelationScore.thenApply(score -> {
            if(score <= 0.3) {
                return 3;
            } else if (score > 0.3 && score <= 0.6) {
                return 2;
            } else {
                return 1;
            }
        });
    }

    @Override
    public String getScoreName() {
        return "TopicRelation";
    }


    private void evaluateTopicRelationScore() {
        CompletableFuture<List<String>> topicCategoryList =
                topicTextCategoriesIbmWatson.getCategoriesFuture()
                        .thenApply(cat -> cat.stream()
                                .map(CategoriesResult::getLabel)
                                .map(this::keepTwoSubcategories)
                                .collect(Collectors.toList()));
        CompletableFuture<List<String>> inputCategoryList =
                inputTextCategoriesIbmWatson.getCategoriesFuture()
                        .thenApply(cat -> cat.stream()
                                .map(CategoriesResult::getLabel)
                                .map(this::keepTwoSubcategories)
                                .collect(Collectors.toList()));
        topicCategoryList.thenAccept(tCategories ->
                inputCategoryList.thenAccept( iCategories -> {
                    List<String> categoryIntersection
                            = ListUtil.intersection(iCategories, tCategories);
                    this.intersectionList = categoryIntersection;
                    topicRelationScore.complete(
                            (double) categoryIntersection.size()
                            / tCategories.size());
                })
        );
    }

    private String keepTwoSubcategories(String fullTopicString) {
        String[] topicArray = fullTopicString.split("/");
        StringBuilder reducedString = new StringBuilder();
        for(int i = 1; i < 3; ++i) {
            if(topicArray.length > i) {
                reducedString.append("/").append(topicArray[i]);
            }
        }
        return reducedString.toString();
    }
}
