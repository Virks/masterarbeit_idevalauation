package mLTopicTestweka;

import weka.classifiers.functions.LibSVM;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.FastVector;
import weka.core.Instances;

import java.io.*;

public class MyFilteredClassifier {

        /**
         * String that stores the text to classify
         */
        String text;
        /**
         * Object that stores the instance.
         */
        Instances instances;
        /**
         * Object that stores the classifier.
         */
        LibSVM classifier;

        /**
         * This method loads the text to be classified.
         * @param fileName The name of the file that stores the text.
         */
        public void load(String fileName) {
            try {
                BufferedReader reader = new BufferedReader(new FileReader(fileName));
                String line;
                text = "";
                while ((line = reader.readLine()) != null) {
                    text = text + " " + line;
                }
                System.out.println("===== Loaded text data: " + fileName + " =====");
                reader.close();
                System.out.println(text);
            }
            catch (IOException e) {
                e.printStackTrace();
                System.out.println("Problem found when reading: " + fileName);
            }
        }

        /**
         * This method loads the model to be used as classifier.
         * @param fileName The name of the file that stores the text.
         */
        public void loadModel(String fileName) {
            try {
                ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
                Object tmp = in.readObject();
                classifier = (LibSVM) tmp;
                in.close();
                System.out.println("===== Loaded model: " + fileName + " =====");
            }
            catch (Exception e) {
                // Given the cast, a ClassNotFoundException must be caught along with the IOException
                e.printStackTrace();
                System.out.println("Problem found when reading: " + fileName);
            }
        }

        /**
         * This method creates the instance to be classified, from the text that has been read.
         */
        public void makeInstance() throws IOException {
            // Create the attributes, class and text
            FastVector fvNominalVal = new FastVector(19);
            fvNominalVal.addElement("alt.atheism");
            fvNominalVal.addElement("comp.graphics");
            fvNominalVal.addElement("comp.os.ms-windows.mis");
            fvNominalVal.addElement("comp.sys.mac.hardware");
            fvNominalVal.addElement("comp.windows.x");
            fvNominalVal.addElement("misc.forsale");
            fvNominalVal.addElement("rec.autos");
            fvNominalVal.addElement("rec.motorcycles");
            fvNominalVal.addElement("rec.sport.baseball");
            fvNominalVal.addElement("rec.sport.hockey");
            fvNominalVal.addElement("sci.crypt");
            fvNominalVal.addElement("sci.electronics");
            fvNominalVal.addElement("sci.med");
            fvNominalVal.addElement("sci.space");
            fvNominalVal.addElement("soc.religion.christian");
            fvNominalVal.addElement("talk.politics.guns");
            fvNominalVal.addElement("talk.politics.mideast");
            fvNominalVal.addElement("talk.politics.misc");
            fvNominalVal.addElement("talk.religion.misc");
            fvNominalVal.addElement("comp.sys.ibm.pc.hardware");
            Attribute attribute1 = new Attribute("topic", fvNominalVal);
            // Create list of instances with one element
            FastVector fvWekaAttributes = new FastVector(1002);

            File txt = new File("wekaDatasets/generated.arff");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(txt)));
            int counter=0;
            fvWekaAttributes.addElement(attribute1);
            while (reader.ready()) {
                String line = reader.readLine();
                if(line.startsWith("@attribute")) {
                    String attribute = line.replace("@attribute ", "").replace(" numeric", "");
                    if(!attribute.startsWith("topic {alt.atheism,comp.gr")) {
                        counter++;
                        fvWekaAttributes.addElement(new Attribute(attribute));
                        if(counter == 1001) {
                           break;
                        }
                    }
                }
            }
            instances = new Instances("Test relation", fvWekaAttributes, 1002);
            // Set class index
            instances.setClassIndex(0);
            // Create and add the instance
            DenseInstance instance = new DenseInstance(1002);
            for(Object yoyo : fvWekaAttributes) {
                instance.setValue((Attribute)yoyo, countWordsUsingStringTokenizer(text, ((Attribute) yoyo).name()));
            }
            // instance.setValue((Attribute)fvWekaAttributes.elementAt(1), text);
            instances.add(instance);
            System.out.println("===== Instance created with reference dataset =====");
            System.out.println(instances);
        }

        /**
         * This method performs the classification of the instance.
         * Output is done at the command-line.
         */
        public void classify() {
            try {
                double pred = classifier.classifyInstance(instances.instance(0));
                System.out.println("===== Classified instance =====");
                System.out.println("Class predicted: " + instances.classAttribute().value((int) pred));
            }
            catch (Exception e) {
                e.printStackTrace();
                System.out.println("Problem found when classifying the text");
            }
        }

    public int countWordsUsingStringTokenizer(String sentence, String word) {
        // split the string by spaces in a
        String a[] = sentence.split(" ");

        // search for pattern in a
        int count = 0;
        for (int i = 0; i < a.length; i++)
        {
            // if match found increase count
            if (word.equals(a[i]))
                count++;
        }

        return count;
    }
        /**
         * Main method. It is an example of the usage of this class.
         * @param args Command-line arguments: fileData and fileModel.
         */
        public static void main (String[] args) throws IOException {
            MyFilteredClassifier classifier;
                classifier = new MyFilteredClassifier();
                classifier.load("test.txt");
                classifier.loadModel("wekaModel.model");
                classifier.makeInstance();
                classifier.classify();
        }

}
