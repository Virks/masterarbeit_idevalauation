package mLTopicTestweka;

import weka.core.*;

import java.io.*;

public class TextDirectoryToArff {

    public Instances createDataset(String directoryPath) throws Exception {

        FastVector atts = new FastVector(2);
        atts.addElement(new Attribute("topic", (FastVector) null));
        atts.addElement(new Attribute("contents", (FastVector) null));
        Instances data = new Instances("text_files_in_" + directoryPath, atts, 0);

        File dir1 = new File(directoryPath);
        String[] dirs = dir1.list();
        for (int j= 0; j < dirs.length; j++) {
            File dir = new File(directoryPath + File.separator+ dirs[j]);
            String[] files = dir.list();
        for (int i = 0; i < files.length; i++) {
            if (files[i].endsWith("")) {
                try {
                    double[] newInst = new double[2];
                    newInst[0] = (double)data.attribute(0).addStringValue(dirs[j]);
                    File txt = new File(directoryPath + File.separator + dirs[j] + File.separator + files[i]);
                    InputStreamReader is;
                    is = new InputStreamReader(new FileInputStream(txt));
                    StringBuffer txtStr = new StringBuffer();
                    int c;
                    while ((c = is.read()) != -1) {
                        txtStr.append((char)c);
                    }
                    newInst[1] = (double)data.attribute(1).addStringValue(txtStr.toString());
                    data.add(new DenseInstance(1.0, newInst));
                } catch (Exception e) {
                    e.printStackTrace();
                    //System.err.println("failed to convert file: " + directoryPath + File.separator + files[i]);
                }
            }}
        }
        return data;
    }

    private static String CLASSIFIER = "comp.graphics";

    public static void main(String[] args) {
        TextDirectoryToArff tdta = new TextDirectoryToArff();
        try {
            Instances dataset = tdta.createDataset("C:\\Users\\mvirk\\Downloads\\20news-18828.tar\\20news-18828");
            System.out.println(dataset);
            BufferedWriter writer = new BufferedWriter(new FileWriter("wekaDatasets/" + CLASSIFIER +".arff"));
            writer.write(dataset.toString());
            writer.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
