import configuraton.ConfigHandler;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.Comparator;
import java.util.Properties;
import java.util.ResourceBundle;

public class AdminController implements Initializable {
    public TableView<Property> propertyTable;
    public TextArea topicInput;
    public TableColumn propertyKeyCol;
    public TableColumn propertyValueCol;

    @Override
        public void initialize(URL url, ResourceBundle resourceBundle) {
        ObservableList<Property> propertiesMap = FXCollections.observableArrayList();
        ConfigHandler.getProperties().forEach((key,value) -> {
            propertiesMap.add(new Property(key.toString(),value.toString()));
        });
        propertyKeyCol.setCellFactory(TextFieldTableCell.forTableColumn());
        propertyValueCol.setCellFactory(TextFieldTableCell.forTableColumn());

        propertyKeyCol.setCellValueFactory(new PropertyValueFactory<>("propertyKey"));
        propertyValueCol.setCellValueFactory(new PropertyValueFactory<>("propertyValue"));
        propertyValueCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<String, String>>() {
            public void handle(TableColumn.CellEditEvent<String, String> t) {
                System.out.println(t);
                propertiesMap.get(t.getTablePosition().getRow()).setPropertyValue(t.getNewValue());
                ConfigHandler.getProperties().setProperty( propertiesMap.get(t.getTablePosition().getRow()).getPropertyKey(), t.getNewValue() );
            }});
        propertyTable.setItems(propertiesMap);

    }

    public void saveProperties(MouseEvent mouseEvent) {
        ConfigHandler.storeProperties();
    }


    public static class Property {

        private final SimpleStringProperty propertyKey;
        private final SimpleStringProperty propertyValue;

        private Property(String propertyKey, String propertyValue) {
            this.propertyKey = new SimpleStringProperty(propertyKey);
            this.propertyValue = new SimpleStringProperty(propertyValue);
        }

        public String getPropertyKey() {
            return propertyKey.get();
        }

        public void setPropertyKey(String fName) {
            propertyKey.set(fName);
        }

        public String getPropertyValue() {
            return propertyValue.get();
        }

        public void setPropertyValue(String fName) {
            propertyValue.set(fName);
        }
    }
}
