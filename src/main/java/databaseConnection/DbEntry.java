package databaseConnection;

import api.reddit.redditData.RedditSubmission;
import api.spellCheck.spellCheckData.FlaggedToken;
import com.ibm.watson.natural_language_understanding.v1.model.CategoriesResult;
import com.ibm.watson.natural_language_understanding.v1.model.KeywordsResult;
import com.ibm.watson.natural_language_understanding.v1.model.SentimentResult;

import java.util.List;

public class DbEntry {
    private String topicText;
    private String inputText;
    private double rarityScore;
    private double originalityScore;
    private double topicRelationScore;
    private double clarityScore;
    private double completenessScore;
    private double socialAcceptabilityScore;
    private int inputTextTotalBingSearchMatches;
    private int topicTextTotalBingSearchMatches;
    private List<KeywordsResult> inputTextKeyWordList;
    private List<KeywordsResult> topicTextKeyWordList;
    private List<KeywordsResult> redditCommentsKeywordList;
    private SentimentResult redditCommentSentimentScore;
    private List<RedditSubmission> redditSubmissionList;
    private List<FlaggedToken> inputTextSpellCheckResult;
    private List<String> inputTextSnippetList;
    private List<String> topicTextSnippetList;
    private List<CategoriesResult> inputTextCategoriesList;
    private List<CategoriesResult> topicTextCategoriesList;
    private List<String> mergedTextSnippetList;
    private int mergedTextTotalBingSearchMatches;
    private String inputTextKeywordsAsString;
    private String topicTextKeyWordListAsString;
    private String topicTextCategoriesListAsString;
    private String inputTextCategoriesListAsString;
    private String keywordsFromSearchesString;

    public int getMergedTextTotalBingSearchMatches() {
        return mergedTextTotalBingSearchMatches;
    }

    public List<String> getMergedTextSnippetList() {
        return mergedTextSnippetList;
    }

    public List<CategoriesResult> getInputTextCategoriesList() {
        return inputTextCategoriesList;
    }

    public List<CategoriesResult> getTopicTextCategoriesList() {
        return topicTextCategoriesList;
    }

    public List<FlaggedToken> getInputTextSpellCheckResult() {
        return inputTextSpellCheckResult;
    }

    public List<String> getInputTextSnippetList() {
        return inputTextSnippetList;
    }

    public List<String> getTopicTextSnippetList() {
        return topicTextSnippetList;
    }

    public String getTopicText() {
        return topicText;
    }

    public String getInputText() {
        return inputText;
    }

    public double getRarityScore() {
        return rarityScore;
    }

    public double getOriginalityScore() {
        return originalityScore;
    }

    public double getTopicRelationScore() {
        return topicRelationScore;
    }

    public double getClarityScore() {
        return clarityScore;
    }

    public double getCompletenessScore() {
        return completenessScore;
    }

    public double getSocialAcceptabilityScore() {
        return socialAcceptabilityScore;
    }

    public int getInputTextTotalBingSearchMatches() {
        return inputTextTotalBingSearchMatches;
    }

    public int getTopicTextTotalBingSearchMatches() {
        return topicTextTotalBingSearchMatches;
    }

    public List<KeywordsResult> getInputTextKeyWordList() {
        return inputTextKeyWordList;
    }

    public List<KeywordsResult> getTopicTextKeyWordList() {
        return topicTextKeyWordList;
    }

    public List<KeywordsResult> getRedditCommentsKeywordList() {
        return redditCommentsKeywordList;
    }

    public SentimentResult getRedditCommentSentimentScore() {
        return redditCommentSentimentScore;
    }

    public List<RedditSubmission> getRedditSubmissionList() {
        return redditSubmissionList;
    }

    public String getInputTextKeywordsAsString() {
        return inputTextKeywordsAsString;
    }

    public String getTopicTextKeyWordListAsString() {
        return topicTextKeyWordListAsString;
    }

    public String getTopicTextCategoriesListAsString() {
        return topicTextCategoriesListAsString;
    }

    public String getInputTextCategoriesListAsString() {
        return inputTextCategoriesListAsString;
    }

    public String getKeywordsFromSearchesString() {
        return keywordsFromSearchesString;
    }

    public static class DbEntryBuilder {
        private String topicText;
        private String inputText;
        private double rarityScore;
        private double originalityScore;
        private double topicRelationScore;
        private double clarityScore;
        private double completenessScore;
        private double socialAcceptabilityScore;
        private int inputTextTotalBingSearchMatches;
        private int topicTextTotalBingSearchMatches;
        private List<KeywordsResult> inputTextKeyWordList;
        private List<KeywordsResult> topicTextKeyWordList;
        private List<KeywordsResult> redditCommentsKeywordList;
        private List<CategoriesResult> inputTextCategoriesList;
        private List<CategoriesResult> topicTextCategoriesList;
        private SentimentResult redditCommentSentimentScore;
        private List<RedditSubmission> redditSubmissionList;
        private List<FlaggedToken> inputTextSpellCheckResult;
        private List<String> inputTextSnippetList;
        private List<String> topicTextSnippetList;
        private List<String> mergedTextSnippetList;
        private int mergedTextTotalBingSearchMatches;
        private String inputTextKeywordsAsString;
        private String topicTextKeyWordListAsString;
        private String topicTextCategoriesListAsString;
        private String inputTextCategoriesListAsString;
        private String keywordsFromSearchesString;

        public DbEntryBuilder setInputTextCategoriesList(List<CategoriesResult> inputTextCategoriesList) {
            this.inputTextCategoriesList = inputTextCategoriesList;
            return this;
        }

        public DbEntryBuilder setTopicTextCategoriesList(List<CategoriesResult> topicTextCategoriesList) {
            this.topicTextCategoriesList = topicTextCategoriesList;
            return this;
        }

        public DbEntryBuilder setInputTextSnippetList(List<String> inputTextSnippetList) {
            this.inputTextSnippetList = inputTextSnippetList;
            return this;
        }

        public DbEntryBuilder setTopicTextSnippetList(List<String> topicTextSnippetList) {
            this.topicTextSnippetList = topicTextSnippetList;
            return this;
        }

        public DbEntryBuilder setInputTextSpellCheckResult(List<FlaggedToken> inputTextSpellCheckResult) {
            this.inputTextSpellCheckResult = inputTextSpellCheckResult;
            return this;
        }

        public DbEntryBuilder setTopicText(String topicText) {
            this.topicText = topicText;
            return this;
        }

        public DbEntryBuilder setInputText(String inputText) {
            this.inputText = inputText;
            return this;
        }

        public DbEntryBuilder setOriginalityScore(double originalityScore) {
            this.originalityScore = originalityScore;
            return this;
        }

        public DbEntryBuilder setTopicRelationScore(double topicRelationScore) {
            this.topicRelationScore = topicRelationScore;
            return this;
        }

        public DbEntryBuilder setClarityScore(double clarityScore) {
            this.clarityScore = clarityScore;
            return this;
        }

        public DbEntryBuilder setCompletenessScore(double completenessScore) {
            this.completenessScore = completenessScore;
            return this;
        }

        public DbEntryBuilder setSocialAcceptabilityScore(double socialAcceptabilityScore) {
            this.socialAcceptabilityScore = socialAcceptabilityScore;
            return this;
        }

        public DbEntryBuilder setRarityScore(double rarityScore) {
            this.rarityScore = rarityScore;
            return this;
        }

        public DbEntryBuilder setInputTextTotalBingSearchMatches(int inputTextTotalBingSearchMatches) {
            this.inputTextTotalBingSearchMatches = inputTextTotalBingSearchMatches;
            return this;
        }

        public DbEntryBuilder setTopicTextTotalBingSearchMatches(int topicTextTotalBingSearchMatches) {
            this.topicTextTotalBingSearchMatches = topicTextTotalBingSearchMatches;
            return this;
        }

        public DbEntryBuilder setInputTextKeyWordList(List<KeywordsResult> inputTextKeyWordList) {
            this.inputTextKeyWordList = inputTextKeyWordList;
            return this;
        }

        public DbEntryBuilder setTopicTextKeyWordList(List<KeywordsResult> topicTextKeyWordList) {
            this.topicTextKeyWordList = topicTextKeyWordList;
            return this;
        }

        public DbEntryBuilder setRedditCommentsKeywordList(List<KeywordsResult> redditCommentsKeywordList) {
            this.redditCommentsKeywordList = redditCommentsKeywordList;
            return this;
        }

        public DbEntryBuilder setRedditCommentSentimentScore(SentimentResult redditCommentSentimentScore) {
            this.redditCommentSentimentScore = redditCommentSentimentScore;
            return this;
        }

        public DbEntryBuilder setRedditSubmissionList(List<RedditSubmission> redditSubmissionList) {
            this.redditSubmissionList = redditSubmissionList;
            return this;
        }

        public DbEntryBuilder setMergedTextSnippetList(List<String> mergedTextSnippetList) {
            this.mergedTextSnippetList = mergedTextSnippetList;
            return this;
        }

        public DbEntryBuilder setMergedTextTotalBingSearchMatches(int mergedTextTotalBingSearchMatches) {
            this.mergedTextTotalBingSearchMatches = mergedTextTotalBingSearchMatches;
            return this;
        }

        public DbEntry build() {
            DbEntry entry = new DbEntry();
            entry.topicText= this.topicText;
            entry.inputText= this.inputText;
            entry.rarityScore= this.rarityScore;
            entry.originalityScore= this.originalityScore;
            entry.topicRelationScore= this.topicRelationScore;
            entry.clarityScore= this.clarityScore;
            entry.completenessScore= this.completenessScore;
            entry.socialAcceptabilityScore= this.socialAcceptabilityScore;
            entry.inputTextTotalBingSearchMatches= this.inputTextTotalBingSearchMatches;
            entry.topicTextTotalBingSearchMatches= this.topicTextTotalBingSearchMatches;
            entry.inputTextKeyWordList= this.inputTextKeyWordList;
            entry.topicTextKeyWordList= this.topicTextKeyWordList;
            entry.redditCommentsKeywordList= this.redditCommentsKeywordList;
            entry.redditCommentSentimentScore= this.redditCommentSentimentScore;
            entry.redditSubmissionList= this.redditSubmissionList;
            entry.inputTextSpellCheckResult = this.inputTextSpellCheckResult;
            entry.inputTextSnippetList = this.inputTextSnippetList;
            entry.topicTextSnippetList = this.topicTextSnippetList;
            entry.inputTextCategoriesList = this.inputTextCategoriesList;
            entry.topicTextCategoriesList = this.topicTextCategoriesList;
            entry.mergedTextSnippetList = this.mergedTextSnippetList;
            entry.mergedTextTotalBingSearchMatches = this.mergedTextTotalBingSearchMatches;
            entry.inputTextKeywordsAsString = this.inputTextKeywordsAsString;
            entry.topicTextKeyWordListAsString = this.topicTextKeyWordListAsString;
            entry.topicTextCategoriesListAsString = this.topicTextCategoriesListAsString;
            entry.inputTextCategoriesListAsString = this.inputTextCategoriesListAsString;
            entry.keywordsFromSearchesString = keywordsFromSearchesString;
            return entry;
        }

        public DbEntryBuilder setInputTextKeywordsAsString(String inputTextKeywordsAsString) {
            this.inputTextKeywordsAsString = inputTextKeywordsAsString;
            return this;
        }

        public DbEntryBuilder setTopicTextKeyWordListAsString(String topicTextKeyWordListAsString) {
            this.topicTextKeyWordListAsString = topicTextKeyWordListAsString;
            return this;
        }

        public DbEntryBuilder setTopicTextCategoriesListAsString(String topicTextCategoriesListAsString) {
            this.topicTextCategoriesListAsString = topicTextCategoriesListAsString;
            return this;
        }

        public DbEntryBuilder setInputTextCategoriesListAsString(String inputTextCategoriesListAsString) {
            this.inputTextCategoriesListAsString = inputTextCategoriesListAsString;
            return this;
        }

        public DbEntryBuilder setKeywordsFromSearchesString(String keywordsFromSearchesString) {
            this.keywordsFromSearchesString = keywordsFromSearchesString;
            return this;
        }
    }
}
