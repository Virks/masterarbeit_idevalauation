package databaseConnection;

import api.reddit.redditData.RedditSubmission;
import com.google.gson.Gson;
import com.ibm.watson.natural_language_understanding.v1.model.CategoriesResult;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

public class DbConnection {
    MongoClient mongoClient;

    public DbConnection() throws UnknownHostException {
        mongoClient = new MongoClient(new ServerAddress("localhost", 27017));
    }

    public void saveEvaluationToDatabase(DbEntry entry) {
        MongoClient mongoClient = new MongoClient(new ServerAddress("localhost", 27017));
        MongoCollection<Document> collection = mongoClient.getDatabase("IdeaEvaluation").getCollection("Ideas");
        Gson gson = new Gson();
        Document doc = new Document()
                .append("topicText", entry.getTopicText())
                .append("inputText", entry.getInputText())
                .append("rarityScore", entry.getRarityScore())
                .append("originalityScore", entry.getOriginalityScore())
                .append("topicRelationScore", entry.getTopicRelationScore())
                .append("clarityScore", entry.getClarityScore())
                .append("completenessScore", entry.getCompletenessScore())
                .append("socialAcceptabilityScore", entry.getSocialAcceptabilityScore())
                .append("inputTextTotalBingSearchMatches", entry.getInputTextTotalBingSearchMatches())
                .append("inputTextBingSnippetList", entry.getInputTextSnippetList())
                .append("topicTextBingSnippetList", entry.getTopicTextSnippetList())
                .append("mergedTextSnippetList", entry.getMergedTextSnippetList())
                .append("topicTextTotalBingSearchMatches", entry.getTopicTextTotalBingSearchMatches())
                .append("mergedTextTotalBingSearchMatches", entry.getMergedTextTotalBingSearchMatches())
                .append("inputTextKeyWordList", gson.toJson(entry.getInputTextKeyWordList()))
                .append("topicTextKeyWordList", gson.toJson(entry.getTopicTextKeyWordList()))
                .append("redditCommentsKeywordList", gson.toJson(entry.getRedditCommentsKeywordList()))
                .append("redditCommentSentimentScore", gson.toJson(entry.getRedditCommentSentimentScore()))
                .append("inputCategories", gson.toJson(entry.getInputTextCategoriesList()))
                .append("topicCategories", gson.toJson(entry.getTopicTextCategoriesList()))
                .append("redditSubmissionList", gson.toJson(entry.getRedditSubmissionList()))
                .append("inputTextSpellCheck", gson.toJson(entry.getInputTextSpellCheckResult()))
                .append("inputKeywordsString", entry.getInputTextKeywordsAsString())
                .append("topicKeywordsString", entry.getTopicTextKeyWordListAsString())
                .append("inputCategoriesString", entry.getInputTextCategoriesListAsString())
                .append("topicCategoriesString", entry.getTopicTextCategoriesListAsString())
                .append("keywordsFromSearch", entry.getKeywordsFromSearchesString());
        collection.insertOne(doc);

    }


//    public static void main(String[] args) throws UnknownHostException {
//        MongoClient mongoClient = new MongoClient(new ServerAddress("localhost", 27017));
//        MongoCollection<Document> collection = mongoClient.getDatabase("IdeaEvaluation").getCollection("Ideas");
//        Document doc = new Document("NoveltyScore", "NoveltyScore")
//                .append("topicTotalMatches", "")
//                .append("type", "database")
//                .append("count", 1)
//                .append("versions", Arrays.asList("v3.2", "v3.0", "v2.6"))
//                .append("info", new Document("x", 203).append("y", 102));
//        collection.insertOne(doc);
//    }


}
