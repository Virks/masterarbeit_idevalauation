package api.reddit.redditData;

import java.util.Date;

public abstract class RedditEntry {

    protected String body;
    protected String retrievedOn;
    public String getBody() {
        return this.body;
    }
    public String getRetrievedOn() {
        return this.retrievedOn;
    }
    public Date getRetrievedOnDate() {
        Date date = new Date();
        date.setTime(Long.parseLong(this.getRetrievedOn())*1000);
        return date;
    }
    protected void setBody(String body) {
        this.body = body;
    }
    protected void setRetrievedOn(String retrievedOn) {
        this.retrievedOn = retrievedOn;
    }
}
