package api.reddit.redditData;

public class RedditComment extends RedditEntry{
    private RedditComment() {
    }

    public static class CommentBuilder {
        String body;
        String retrievedOn;
        private CommentBuilder() {

        }
        public static CommentBuilder getInstance() {
            return new CommentBuilder();
        }

        public CommentBuilder setBody(String body) {
            this.body = body;
            return this;
        }
        public CommentBuilder setRetrievedOn(String retrieved_on) {
            this.retrievedOn = retrieved_on;
            return this;
        }

        public RedditComment build() {
            RedditComment comment = new RedditComment();
            comment.setBody(this.body);
            comment.setRetrievedOn(this.retrievedOn);
            return comment;
        }


    }
}
