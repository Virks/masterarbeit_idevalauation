package api.reddit.redditData;

import java.util.ArrayList;
import java.util.List;

public class RedditSubmission extends RedditEntry {
    private String id;
    private int totalNumberOfSearchResults;
    private int numberOfCommentsInRequest;
    private List<RedditComment> comments = new ArrayList<>();

    private RedditSubmission() {
    }
    private void setId(String id) {
        this.id = id;
    }
    private void setTotalNumberOfSearchResults(int totalSearchResults) {
        this.totalNumberOfSearchResults = totalSearchResults;
    }
    private void setNumberOfCommentsInRequest(int numberOfCommentsInRequest) {
        this.numberOfCommentsInRequest = numberOfCommentsInRequest;
    }

    public int getNumberOfCommentsInRequest() {
        return numberOfCommentsInRequest;
    }

    public String getId() {
        return this.id;
    }
    public int getTotalNumberOfSearchResults() {
        return this.totalNumberOfSearchResults;
    }

    public List<RedditComment> getComments() {
        return comments;
    }

    public void setComments(List<RedditComment> comments) {
        this.comments = comments;
    }

    public static class SubmissionBuilder {
        private String body;
        private String id;
        private int totalNumberOfSearchResults;
        private int numberOfCommentsInRequest;
        private String retrievedOn;

        private SubmissionBuilder() {
        }

        public static SubmissionBuilder getInstance() {
            return new SubmissionBuilder();
        }

        public SubmissionBuilder setBody(String body) {
            this.body = body;
            return this;
        }

        public SubmissionBuilder setId(String id) {
            this.id = id;
            return this;
        }

        public SubmissionBuilder setTotalNumberOfSearchResults(int totalSearchResults) {
            this.totalNumberOfSearchResults = totalSearchResults;
            return this;
        }

        public SubmissionBuilder setRetrievedOn(String retrieved_on) {
            this.retrievedOn = retrieved_on;
            return this;
        }

        public SubmissionBuilder setNumberOfCommentsInRequest(int numberOfCommentsInRequest) {
            this.numberOfCommentsInRequest = numberOfCommentsInRequest;
            return this;
        }

        public RedditSubmission build() {
            RedditSubmission submission = new RedditSubmission();
            submission.setBody(this.body);
            submission.setId(this.id);
            submission.setTotalNumberOfSearchResults(this.totalNumberOfSearchResults);
            submission.setRetrievedOn(this.retrievedOn);
            submission.setNumberOfCommentsInRequest(this.numberOfCommentsInRequest);
            return submission;
        }
    }
}
