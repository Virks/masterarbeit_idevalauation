package api.reddit;

import api.ibmWatson.IbmWatsonAPI;
import api.reddit.entryHandler.IEntryHandler;
import api.reddit.entryHandler.CommentsHandler;
import api.reddit.entryHandler.SubmissionHandler;
import api.reddit.redditData.RedditComment;
import api.reddit.redditData.RedditEntry;
import api.reddit.redditData.RedditSubmission;
import com.google.gson.*;
import com.ibm.watson.natural_language_understanding.v1.model.KeywordsResult;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import util.LoggingFuture;
import util.RetryHandler;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.*;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class RedditSearchAPI {
    private List<String> inputText;
    private CompletableFuture<List<RedditSubmission>> redditSearch = new LoggingFuture<>();
    private static final NameValuePair AGGS_PARAMETER = new BasicNameValuePair("aggs", "created_utc");
    private static final NameValuePair LIMIT_PARAMETER = new BasicNameValuePair("limit", "200");
    private static final NameValuePair BEFORE_PARAMETER = new BasicNameValuePair("before", String.valueOf(System.currentTimeMillis() / 1000));
    private static final NameValuePair AFTER_PARAMETER = new BasicNameValuePair("after", "0");
    private static final NameValuePair SORT_PARAMETER = new BasicNameValuePair("sort", "desc");
    private static final NameValuePair SORTTYPE_PARAMETER = new BasicNameValuePair("sort_type","num_comments");
    private static final NameValuePair NUMCOMMENTS_PARAMETER = new BasicNameValuePair("num_comments", ">0");
    private static final NameValuePair REDDIT_PARAM_SORT = new BasicNameValuePair("sort", "relevance"); /*comments or relevance or top*/
    private static final NameValuePair REDDIT_LIMIT_PARAM = new BasicNameValuePair("limit", "50");
    private static final NameValuePair REDDIT_T_PARAM = new BasicNameValuePair("t", "all");
    private static final NameValuePair REDDIT_TYPE_PARAM = new BasicNameValuePair("type", "link");
    Map<String, RedditSubmission> foundRedditSubmissions = new ConcurrentHashMap<>();

    public RedditSearchAPI startSearch(CompletableFuture<List<KeywordsResult>> inputTextFuture) {
        inputTextFuture.thenApply(inputText -> inputText.stream().map(KeywordsResult::getText).collect(Collectors.toList())).thenAccept(this::startSearch);
        return this;
    }

    public RedditSearchAPI startSearch(List<String> inputText) {
        this.inputText = inputText;
        LoggingFuture<List<RedditSubmission>> redditSubmissionsFuture = new LoggingFuture<>();

        this.getSubmissionsForSearchQuery( redditSubmissionsFuture);
        redditSubmissionsFuture.thenAccept( submissionss ->  {
            int countSubmissionsWithComments = 0;
            for(RedditSubmission redditSub : submissionss) {
                foundRedditSubmissions.put(redditSub.getId(), redditSub);
            }
            List<RedditSubmission> submissions = new ArrayList<>(foundRedditSubmissions.values());
            for(RedditSubmission redsub : submissions) {
                if(redsub.getNumberOfCommentsInRequest() > 0) {
                    countSubmissionsWithComments++;
                }
            }
            if(!submissions.isEmpty() && countSubmissionsWithComments > 30) {
                List<LoggingFuture<List<RedditComment>>> redditCommmentsFutures = new CopyOnWriteArrayList<>();
                submissions.forEach(submission -> {
                    LoggingFuture<List<RedditComment>> redditCommmentsFuture = new LoggingFuture<>();
                    redditCommmentsFutures.add(redditCommmentsFuture);
                    new Thread(() ->this.getCommentsForSubmission(submission, redditCommmentsFuture)).start();
                    redditCommmentsFuture.thenAccept(comments -> {
                        submission.setComments(comments);
                        int counter = 0;
                        for (LoggingFuture<List<RedditComment>> future : redditCommmentsFutures) {
                            if (future.isDone()) {
                                counter++;
                            }
                        }
                        if (counter == submissions.size()) {
                            redditSearch.complete(submissions);
                        }
                    });
                });
            } else if(countSubmissionsWithComments <= 30 && inputText.size() > 1) {
                System.out.println("start another search: " + inputText + " found "+ countSubmissionsWithComments + " with comments!");
                inputText.remove(inputText.size() - 1);
                startSearch(inputText);
            } else if(inputText.size() == 1) {
                if(new StringTokenizer(inputText.get(0)).countTokens() > 1) {
                   List<String> inputString = Collections.list(new StringTokenizer(inputText.get(0))).stream()
                            .map(token -> (String) token)
                            .collect(Collectors.toList());
                    inputString.remove(inputString.size()-1);
                    startSearch(inputString);
                } else {
                    redditSearch.complete(submissions);
                }
            } else {
                redditSearch.complete(submissions);
            }
        });
        return this;
    }

    public CompletableFuture<List<RedditSubmission>> getSearchFuture() {
        return this.redditSearch;
    }

    public static double getAverageCommentTimeToSubmission(List<RedditSubmission> redditSubmissions) {
        double differenceOfAnswerAndCreatiionInMinutes = 0;
        double counter = 0;

        for(RedditSubmission submission : redditSubmissions) {
            for(RedditComment comment : submission.getComments()) {
                Date submissionDate = new Date();
                //multiply the timestampt with 1000 as java expects the time in milliseconds
                submissionDate.setTime(Long.parseLong(submission.getRetrievedOn())*1000);
                Date commentDate = new Date ();
                commentDate.setTime(Long.parseLong(comment.getRetrievedOn())*1000);
                differenceOfAnswerAndCreatiionInMinutes += ( (commentDate.getTime() - submissionDate.getTime()) / ((double)1000 * 60) );
                counter++;
            }
        };
        return differenceOfAnswerAndCreatiionInMinutes/counter;
    }

    public String transformSubmissionCommentsToString(List<RedditSubmission> submissionList) {
        StringBuilder builder = new StringBuilder();
        submissionList.forEach(submission -> submission.getComments().forEach(comment ->
            builder.append(comment.getBody()).append(" ")
        ));
        return builder.toString();
    }
    private void getSubmissionsForSearchQuery(
            CompletableFuture<List<RedditSubmission>> postsFuture) {
        CompletableFuture<List<RedditEntry>> responseFuture
                = new CompletableFuture<>();
        IEntryHandler submissionHandler = new SubmissionHandler(inputText);
        try {
            // public URIBuilder getUriBuilder() throws URISyntaxException {
            //     URIBuilder builder = new URIBuilder(
            //             "https://www.reddit.com/search.json");
            //     builder.setParameters(REDDIT_PARAM_SORT,
            //             REDDIT_LIMIT_PARAM,
            //             REDDIT_T_PARAM,
            //             REDDIT_TYPE_PARAM)
            //             .setParameter("q", this.inputText));
            //     return builder;
            // }
            URIBuilder builder = submissionHandler.getUriBuilder();
            new Thread(()->getRequestForRedditEntrys(responseFuture,
                    builder, submissionHandler)).start();
            responseFuture.thenApply(entries -> entries.stream()
                        .map(entry -> (RedditSubmission) entry)
                        .collect(Collectors.toList()))
                    .thenAccept(postsFuture::obtrudeValue);
        } catch(URISyntaxException e) {
            e.printStackTrace();
            postsFuture.complete(new ArrayList<>());
        }
    }

    private void getCommentsForSubmission(RedditSubmission submission,
            CompletableFuture<List<RedditComment>> commentsFuture) {
        if(submission.getNumberOfCommentsInRequest() == 0) {
            commentsFuture.complete(new ArrayList<>());
            return;
        }
        CompletableFuture<List<RedditEntry>> responseFuture
                = new CompletableFuture<>();
        IEntryHandler commentsHandler = new CommentsHandler(submission);
        try {
            // public URIBuilder getUriBuilder() throws URISyntaxException {
            //     return new URIBuilder(
            //             "https://www.reddit.com/comments/"
            //                     + this.submission.getId() + ".json");
            // }
            URIBuilder builder = commentsHandler.getUriBuilder();
            getRequestForRedditEntrys(responseFuture
                    , builder, commentsHandler);
            responseFuture.thenApply(entries -> entries.stream()
                        .map(entry -> (RedditComment) entry)
                        .collect(Collectors.toList()))
                    .thenAccept(commentsFuture::obtrudeValue);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            commentsFuture.complete(new ArrayList<>());
        }
    }

    private void getRequestForRedditEntrys(
            CompletableFuture<List<RedditEntry>> bodyFuture,
            URIBuilder builder, IEntryHandler entryHandler) {
        try (CloseableHttpClient  client = HttpClientBuilder.create()
            .setRetryHandler(new RetryHandler())
            .addInterceptorLast((HttpResponseInterceptor)
                (response, context) -> {
                    if(response.getStatusLine().getStatusCode() > 299
                          || response.getStatusLine().getStatusCode() < 199) {
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        throw new InterruptedIOException("Bad Status");
                    } else {
                        try {
                            List<RedditEntry> entryList = entryHandler
                                    .parseRedditEntries(response);
                            bodyFuture.complete(entryList);
                        } catch (JsonSyntaxException e) {
                            throw new InterruptedIOException("Bad Entry...");
                        }
                    }
                })
            .build()) {
          client.execute(new HttpGet(builder.build()));
        } catch ( URISyntaxException | IOException e) {
            e.printStackTrace();
            bodyFuture.complete(new ArrayList<>());
        }
    }


    /**
     *
     *
     *
      _______  _    _________ _______  _______  _        _______ __________________          _______
     (  ___  )( \   \__   __/(  ____ \(  ____ )( (    /|(  ___  )\__   __/\__   __/|\     /|(  ____ \
     | (   ) || (      ) (   | (    \/| (    )||  \  ( || (   ) |   ) (      ) (   | )   ( || (    \/
     | (___) || |      | |   | (__    | (____)||   \ | || (___) |   | |      | |   | |   | || (__
     |  ___  || |      | |   |  __)   |     __)| (\ \) ||  ___  |   | |      | |   ( (   ) )|  __)
     | (   ) || |      | |   | (      | (\ (   | | \   || (   ) |   | |      | |    \ \_/ / | (
     | )   ( || (____/\| |   | (____/\| ) \ \__| )  \  || )   ( |   | |   ___) (___  \   /  | (____/\
     |/     \|(_______/)_(   (_______/|/   \__/|/    )_)|/     \|   )_(   \_______/   \_/   (_______/
     *
     * Alternatives for reddit
     *
     */
    private void  getRequestForRedditEntrys(LoggingFuture<List<RedditEntry>> bodyFuture, RedditSubmission submission) {
        boolean isComment = submission != null;
        AtomicBoolean retryWithRedditApi = new AtomicBoolean(false);
        AtomicBoolean failedWithPushshitft = new AtomicBoolean(false);
//        try {
//            if(isComment) {
//                URIBuilder builder2 = new URIBuilder("https://www.reddit.com/comments/" + submission.getId() + ".json");
//                bodyFuture.complete(addCommentsToList(getRequestWithHttpClient(builder2).body()));
//                IdeatorLogger.debug("bodyFuture completed");
//                return;
//            }
//        } catch (URISyntaxException | InterruptedException | JsonSyntaxException | IOException e ) {
//            IdeatorLogger.debug("entering retry loop");
//        }
        URIBuilder builder;
        try {
            if(submission != null) {
                 builder = new URIBuilder("https://api.pushshift.io/reddit/comment/search")
//                        .setParameter("limit", "300")
                        .setParameter("link_id", submission.getId());
            } else {
                System.out.println("builder.toString()");
                builder = new URIBuilder("https://api.pushshift.io/reddit/submission/search");
                System.out.println(builder.toString());
                builder.setParameters(LIMIT_PARAMETER, BEFORE_PARAMETER,AFTER_PARAMETER,SORT_PARAMETER,AGGS_PARAMETER,SORTTYPE_PARAMETER)
                        .setParameter("q", IbmWatsonAPI.transformStringListToString(inputText));
            }
        } catch (Exception e) {
            e.printStackTrace();
            bodyFuture.complete(new ArrayList<>());
            return;
        }

        try (CloseableHttpClient  client = HttpClientBuilder.create()
                .setRetryHandler(new RetryHandler())
                .addInterceptorLast((HttpResponseInterceptor) (response, context) -> {
                    if (response.getStatusLine().getStatusCode() == 429 || failedWithPushshitft.get() || retryWithRedditApi.get()) {
                        String body = "";
                        try {
                            if(isComment) {
                                retryWithRedditApi.set(true);
                                URIBuilder builder2 = new URIBuilder("https://www.reddit.com/comments/" + submission.getId() + ".json");
                                body = getRequestWithHttpClient(builder2).body();
                                List<RedditEntry> entryList = new ArrayList<>();
                                addCommentsToList(entryList, body);
                                bodyFuture.complete(entryList);
                            } else {
                                throw new InterruptedIOException("got bad Status code Retying...");
                            }
                            return;
                        } catch (URISyntaxException | InterruptedException  | JsonSyntaxException e ) {
                            e.printStackTrace();
                        }
                        try {
                            Thread.sleep(500);
                            System.out.println("retrying with pushshift");
                            retryWithRedditApi.set(false);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        throw new InterruptedIOException("got 429 Retying...");
                    } else if(response.getStatusLine().getStatusCode() > 299 || response.getStatusLine().getStatusCode() < 199) {
                        failedWithPushshitft.set(!failedWithPushshitft.get());
                        retryWithRedditApi.set(!retryWithRedditApi.get());
                        throw new InterruptedIOException("got bad Status code Retying...");
                    } else {
                        Scanner sc = new Scanner(response.getEntity().getContent());
                        StringBuilder strBuilder = new StringBuilder();
                        while (sc.hasNext()) {
                            strBuilder.append(sc.nextLine());
                        }
                        try {
                            List<RedditEntry> entryList = addEntriesToList(strBuilder.toString(), isComment);
                            bodyFuture.complete(entryList);
                        } catch (JsonSyntaxException e) {
                            failedWithPushshitft.set(true);
                            throw new InterruptedIOException("Bad Entry...");
                        }
                    }
                })
                .build()) {
            client.execute(new HttpGet(builder.build().toString()));
        } catch ( URISyntaxException e) {
            e.printStackTrace();
        } catch ( IOException e) {
            e.printStackTrace();
            System.out.println("Retrying Reddit Request");
        }
    }

    private void getRedditEntries(
            CompletableFuture<List<RedditEntry>> bodyFuture,
            RedditSubmission submission) {
        boolean getCommentsForEntry = submission != null;
        URIBuilder builder;
        try {
            if (submission != null) {
                builder = new URIBuilder(
                        "https://api.pushshift.io/reddit/comment/search")
                        .setParameter("link_id", submission.getId());
            } else {
                builder = new URIBuilder(
                        "https://api.pushshift.io/reddit/submission/search")
                        .setParameters(
                            LIMIT_PARAMETER,
                            BEFORE_PARAMETER,
                            AFTER_PARAMETER,
                            SORT_PARAMETER,
                            AGGS_PARAMETER,
                            SORTTYPE_PARAMETER)
                        .setParameter("q", IbmWatsonAPI.transformStringListToString(inputText));
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
            bodyFuture.complete(new ArrayList<>());
            return;
        }
        try (CloseableHttpClient client = HttpClientBuilder.create()
                .setRetryHandler(new RetryHandler())
                .addInterceptorLast((HttpResponseInterceptor)
                        (response, context) -> {
                    if (response.getStatusLine().getStatusCode() == 429) {
                        String body = "";
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        throw new InterruptedIOException("got 429 Retying...");
                    } else {
                        String responseStr =
                                EntityUtils.toString(response.getEntity());
                        List<RedditEntry> entryList =
                                addEntriesToList(responseStr,
                                        getCommentsForEntry);
                        bodyFuture.complete(entryList);
                    }
                })
                .build()) {
            client.execute(new HttpGet(builder.build()));
        } catch (URISyntaxException | IOException e) {
            bodyFuture.complete(new ArrayList<>());
            e.printStackTrace();
        }
    }

    private List<RedditEntry> addEntriesToList(String body, boolean isComment) throws InterruptedIOException {
        List<RedditEntry> commentList = new ArrayList<>();

        if (body != null && !body.isEmpty()) {
            JsonElement bodyElement = JsonParser.parseString(body);
            if (bodyElement.isJsonObject()) {
                JsonObject searchResponseJson = bodyElement.getAsJsonObject();
                readEntriesFromDataElement(isComment, commentList, searchResponseJson);
            }
        }

        return commentList;
    }

    private void addCommentsToList(List<RedditEntry> commentList, String body) {
        if (body != null && !body.isEmpty()) {

            JsonElement bodyElement = JsonParser.parseString(body);
            if (bodyElement.isJsonArray()) {
                JsonArray searchResponseJson = bodyElement.getAsJsonArray();
                searchResponseJson.forEach( entry -> {
                    if(entry.isJsonObject()) {
                        readCommentsFromRedditApi(commentList, entry.getAsJsonObject());
                    }
                });
            }
        }
    }
    private void readSubmissionFromRedditApi(List<RedditEntry> submissionList, String body) {
        if (body != null && !body.isEmpty()) {

            JsonElement entry = JsonParser.parseString(body);
            if (entry.isJsonObject()) {
                JsonObject entryObject = entry.getAsJsonObject();
                JsonElement dataElement = entryObject.get("data");
                if (dataElement.isJsonObject() && dataElement.getAsJsonObject().has("children")) {
                    JsonElement childrenElement = dataElement.getAsJsonObject().get("children");
                    if (childrenElement.isJsonArray()) {
                        JsonArray childrenArray = childrenElement.getAsJsonArray();
                        childrenArray.forEach(child -> {
                            if (child.isJsonObject()) {
                                JsonObject commentObject = child.getAsJsonObject();
                                JsonElement childDataElement = commentObject.get("data");
                                if (childDataElement.isJsonObject()) {
                                    JsonObject childDataObject = childDataElement.getAsJsonObject();
                                    String created_utc = childDataObject.get("created_utc").getAsString();
                                    if (created_utc.contains(".")) {
                                        created_utc = created_utc.substring(0, created_utc.indexOf("."));
                                    }
                                    String entryBody = childDataObject.get("title").getAsString() + childDataObject.get("selftext").getAsString();

                                    RedditSubmission redditsubmission = RedditSubmission.SubmissionBuilder.getInstance()
                                            .setNumberOfCommentsInRequest(childDataObject.get("num_comments").getAsInt())
                                            .setId(childDataObject.get("id").getAsString())
                                            .setBody(entryBody)
                                            .setRetrievedOn(created_utc)
                                            .build();
                                    submissionList.add(redditsubmission);
                                }
                            }
                        });
                    }
                }
            }
        }
    }
    private void readCommentsFromRedditApi(List<RedditEntry> commentList, JsonObject entry) {
        if(entry.isJsonObject()) {
            JsonObject entryObject = entry.getAsJsonObject();
            JsonElement dataElement = entryObject.get("data");
            if(dataElement.isJsonObject() && dataElement.getAsJsonObject().has("children")) {
                JsonElement childrenElement = dataElement.getAsJsonObject().get("children");
                if (childrenElement.isJsonArray()) {
                    JsonArray childrenArray = childrenElement.getAsJsonArray();
                    childrenArray.forEach(child -> {
                        if (child.isJsonObject() ) {
                            JsonObject commentObject = child.getAsJsonObject();
                            JsonElement childDataElement = commentObject.get("data");
                            if(childDataElement.isJsonObject() && childDataElement.getAsJsonObject().has("link_id")) {
                                JsonObject childDataObject = childDataElement.getAsJsonObject();
                                String created_utc = childDataObject.get("created_utc").getAsString();
                                if(created_utc.contains(".")) {
                                    created_utc = created_utc.substring(0, created_utc.indexOf("."));
                                }
                                RedditEntry redditComment = RedditComment.CommentBuilder.getInstance()
                                        .setBody(childDataObject.get("body").getAsString())
                                        .setRetrievedOn(created_utc)
                                        .build();
                                commentList.add(redditComment);
                                if(childDataObject.has("replies") && childDataObject.get("replies").isJsonObject()) {
                                    JsonObject replyObject = childDataObject.get("replies").getAsJsonObject();
                                    readCommentsFromRedditApi(commentList,replyObject);
                                }
                            }
                        }
                    });
                }
            }
        }
    }

    private void readEntriesFromDataElement(boolean isComment, List<RedditEntry> commentList, JsonObject searchResponseJson) {
        JsonElement data = searchResponseJson.get("data");
        if (data.isJsonArray()) {
            data.getAsJsonArray().forEach(element -> {
                if (element.isJsonObject()) {
                    JsonObject object = element.getAsJsonObject();
                    RedditEntry entry;
                    // a provided submission means that comments are getting fetched for it
                    if(isComment) {
                            entry = RedditComment.CommentBuilder.getInstance()
                                    .setBody(object.get("body").getAsString())
                                    .setRetrievedOn(object.has("created_utc") ? object.get("created_utc").getAsString() : "")
                                    .build();
                            if(entry.getBody().equals("[deleted]") || entry.getBody().equals("[moved]") ) {
                                return;
                            }
                    } else {
                        int totalSearchResults = readTotalSearchResultFromAggs(searchResponseJson);
                        entry = RedditSubmission.SubmissionBuilder.getInstance()
                                .setBody(object.has("body") ? object.get("body").toString() : (object.has("selftext") ? object.get("selftext").getAsString() : ""))
                                .setId(object.has("id") ? object.get("id").getAsString() : "")
                                .setRetrievedOn(object.has("created_utc") ? object.get("created_utc").getAsString() : "")
                                .setNumberOfCommentsInRequest(object.has("num_comments") ? Integer.parseInt(object.get("num_comments").getAsString()):0)
                                .setTotalNumberOfSearchResults(totalSearchResults)
                                .build();
                    }
                    commentList.add(entry);
                }
            });
        }
    }
    private HttpResponse<String> getRequestWithHttpClient(URIBuilder builder) throws URISyntaxException, IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(builder.build())
                .timeout(Duration.ofMinutes(1))
                .header("Content-Type", "application/json")
                .build();
        return client.send(request, HttpResponse.BodyHandlers.ofString());
    }

    private int readTotalSearchResultFromAggs(JsonObject searchResponseJson) {
        int totalSearchResults = 0;
        JsonElement aggs = searchResponseJson.get("aggs");
        if(aggs.isJsonObject() && aggs.getAsJsonObject().has("created_utc")) {
            JsonElement createdutc = aggs.getAsJsonObject().get("created_utc");
            if(createdutc.isJsonArray()) {
                for(JsonElement doc : createdutc.getAsJsonArray()) {
                    if(doc.isJsonObject()) {
                        totalSearchResults += Integer.parseInt(doc.getAsJsonObject().get("doc_count").toString());
                    }
                }
            }
        }
        return totalSearchResults;
    }

    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>();
        stringList.add("CO2 Anzeige");

        RedditSearchAPI search = new RedditSearchAPI().startSearch(stringList);
        search.getSearchFuture().thenAccept( dsds -> {
            System.out.println(dsds);
        });

    }
}
