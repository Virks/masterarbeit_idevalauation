package api.textAnalysis;

import com.google.gson.*;
import configuraton.ConfigHandler;
import org.apache.http.client.utils.URIBuilder;
import util.LoggingFuture;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;


public class MicrosoftTextAnalysis {
    private final Documents documents;
    private LoggingFuture<List<String>> keyPhrasesFuture = new LoggingFuture<>();
    private LoggingFuture<Double> sentientFuture = new LoggingFuture<>();

    public MicrosoftTextAnalysis(String inputText) {
        this.documents = new Documents();
        this.documents.add("1", inputText);
        this.extractKeyPhrases();
        this.sentientAnalysis();
    }

    public LoggingFuture<List<String>> getKeyPhrasesFuture() {
        return keyPhrasesFuture;
    }

    public LoggingFuture<Double> getSentientFuture() {
        return sentientFuture;
    }

    public void extractKeyPhrases() {
        requestTextAnaylsisAPI(ConfigHandler.getMicrosoftTextAnalysisAPIKeyPhrasesPath());
    }


    public void sentientAnalysis() {
        requestTextAnaylsisAPI(ConfigHandler.getMicrosoftTextAnalysisAPISentientPath());
    }

    private double getAverageScoreFromSentientResponse(String body) {
        double score = 0;
        int counter = 0;
        JsonArray documentsArray = readDocumentsArrayFromJson(body);
        for(JsonElement documentElement : documentsArray) {
            if (documentElement.isJsonObject()) {
                JsonObject documentObject = documentElement.getAsJsonObject();
                if(documentObject.has("score")) {
                    score += documentObject.get("score").getAsDouble();
                    counter++;
                }
            }
        }
        return score/counter;
    }

    private List<String> getKeywordsFromKeyPhrasesResponse(String body) {
        List<String> keywordList = new ArrayList<>();
        JsonArray documentsArray = readDocumentsArrayFromJson(body);
        for(JsonElement documentElement : documentsArray) {
            if (documentElement.isJsonObject()) {
                JsonObject documentObject = documentElement.getAsJsonObject();
                if(documentObject.has("keyPhrases") && documentObject.get("keyPhrases").isJsonArray()) {
                    documentObject.get("keyPhrases").getAsJsonArray().forEach( keyPhrase -> {
                        keywordList.add(keyPhrase.getAsString());
                    });
                }
            }
        }
        return keywordList;
    }

    private JsonArray readDocumentsArrayFromJson(String jsonString) {
        try {
            if (jsonString != null && !jsonString.isEmpty()) {
                JsonElement bodyElement = JsonParser.parseString(jsonString);
                if (bodyElement.isJsonObject()) {
                    JsonObject searchResponseJson = bodyElement.getAsJsonObject();
                    JsonElement documentElements = searchResponseJson.get("documents");
                    if (documentElements.isJsonArray()) {
                        return documentElements.getAsJsonArray();
                    }
                }
            }
        }catch (JsonSyntaxException e) {
            System.out.println("text Analysis API didnt return a JSON document: " + jsonString);
        }
        return new JsonArray();
    }

    private void requestTextAnaylsisAPI( String apiType) {
        Thread request = new Thread(() -> {
            try {
                String text = new Gson().toJson(documents);
                byte[] encoded_text = text.getBytes(StandardCharsets.UTF_8);
                URIBuilder builder = new URIBuilder(ConfigHandler.getMicrosoftTextAnalysisAPIHost() + ConfigHandler.getMicrosoftTextAnalysisAPIPath() + apiType);
                builder.setParameter("showStats", "true");
                URL url = new URL(builder.build().toString());
                HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "text/json");
                connection.setRequestProperty("Ocp-Apim-Subscription-Key", ConfigHandler.getMicrosoftTextAnalysisAPISubscriptionKey());
                connection.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.write(encoded_text, 0, encoded_text.length);
                wr.flush();
                wr.close();
                StringBuilder response = new StringBuilder();
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String line;
                while ((line = in.readLine()) != null) {
                    response.append(line);
                }
                in.close();
                if(ConfigHandler.getMicrosoftTextAnalysisAPISentientPath().equals(apiType)) {
                    this.sentientFuture.complete(getAverageScoreFromSentientResponse(response.toString()));
                } else {
                    this.keyPhrasesFuture.complete(getKeywordsFromKeyPhrasesResponse(response.toString()));
                }
            } catch (URISyntaxException | IOException e) {
                if(ConfigHandler.getMicrosoftTextAnalysisAPISentientPath().equals(apiType)) {
                    this.sentientFuture.complete(0.0);
                } else {
                    this.keyPhrasesFuture.complete(new ArrayList<>());
                }
                e.printStackTrace();
            }
        });
        request.start();
    }


    private class Document {
        public String id, text;

        public Document(String id, String text){
            this.id = id;
            this.text = text;
        }
    }
    private class Documents {
        public List<Document> documents;

        public Documents() {
            this.documents = new ArrayList<>();
        }
        public void add(String id, String text) {
            this.documents.add (new Document (id, text));
        }
    }
}
