package api.microsoftSearch;

import api.microsoftSearch.data.SearchResponse;
import configuraton.ConfigHandler;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import util.LoggingFuture;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.CompletableFuture;

public class BingSearchAPI {

    public static final String APPLICATION_JSON = "application/json";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String OCP_APIM_SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";
    private String inputText;
    private LoggingFuture<List<String>> snippetListFuture = new LoggingFuture<>();
    private LoggingFuture<Map<String,Integer>> topWordsFuture = new LoggingFuture<>();
    private LoggingFuture<Integer> totalEstimatedMatchesFuture = new LoggingFuture<>();


    public BingSearchAPI() {
        searchResponse.thenAcceptAsync( response -> {
            snippetListFuture.complete(response.getSnippetsFromSeach());
            topWordsFuture.complete(response.getTopThreeMostUsedWordsInSnippets());
            totalEstimatedMatchesFuture.complete(response.getTotalEstimatedMatches());
        });
    }

    public BingSearchAPI startBingSearch(String inputText) {
        this.inputText = inputText;
        getBingSearchResults(inputText);
        return this;
    }

    public BingSearchAPI startBingSearch(CompletableFuture<String> inputTextFuture) {
        inputTextFuture.thenAcceptAsync( inputText -> {
            this.inputText = inputText;
            getBingSearchResults(inputText);
        });
        return this;
    }

    public LoggingFuture<List<String>> getSnippetListFuture() {
        return snippetListFuture;
    }

    public CompletableFuture<String> transformSnippetsToString(CompletableFuture<List<String>> snippetListFut1,CompletableFuture<List<String>> snippetListFut2, CompletableFuture<List<String>> snippetListFut3) {
        CompletableFuture<String> snipFut = new CompletableFuture<>();
        snippetListFut1.thenAccept(snippetList1 -> {
            snippetListFut2.thenAccept(snippetList2 -> {
                snippetListFut3.thenAccept(snippetList3 -> {
                    StringBuilder builder = new StringBuilder();
                    for(String snippet : snippetList1) {
                        builder.append(snippet).append(" ");
                    }
                    for(String snippet : snippetList2) {
                        builder.append(snippet).append(" ");
                    }
                    for(String snippet : snippetList3) {
                        builder.append(snippet).append(" ");
                    }
                    snipFut.complete(builder.toString());
                });
            });
        });
        return snipFut;
    }

    public LoggingFuture<Map<String, Integer>> getTopWordsFuture() {
        return topWordsFuture;
    }

    public LoggingFuture<Integer> getTotalEstimatedMatchesFuture() {
        return totalEstimatedMatchesFuture;
    }





//    private void getBingSearchResults(String searchQuery)  {
//        URIBuilder builder;
//        try {
//            String msftEndpoint = ConfigHandler.getBingCustomSearchAPIhost()
//                    + ConfigHandler.getBingCustomSearchAPIpath();
//            builder = new URIBuilder(msftEndpoint);
//            builder.setParameter("q", searchQuery);
//
//            searchResponse.complete(
//                    new SearchResponse(dispatchRequest(builder), inputText));
//        } catch (URISyntaxException |InterruptedException | IOException e) {
//            e.printStackTrace();
//            searchResponse.complete(new SearchResponse("", inputText));
//        }
//    }
//
//    private String dispatchRequest(URIBuilder builder)
//            throws URISyntaxException, IOException, InterruptedException {
//        HttpClient client = HttpClient.newHttpClient();
//        HttpRequest request = HttpRequest.newBuilder()
//                .uri(builder.build())
//                .timeout(Duration.ofMinutes(1))
//                .header(OCP_APIM_SUBSCRIPTION_KEY,
//                        ConfigHandler.getBingCustomSearchAPIsubscriptionKey())
//                .header(CONTENT_TYPE, APPLICATION_JSON)
//                .build();
//        return client.send(request,
//                HttpResponse.BodyHandlers.ofString()).body();
//    }
    private LoggingFuture<SearchResponse> searchResponse
        = new LoggingFuture<>();

    private void getBingSearchResults(String inputText) {
        try(CloseableHttpClient httpclient = HttpClients.createDefault())
        {
            String msftEndpoint = ConfigHandler.getBingCustomSearchAPIhost()
                    + ConfigHandler.getBingCustomSearchAPIpath();
            URIBuilder builder = new URIBuilder(msftEndpoint);

            builder.setParameter("q", inputText);

            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            request.setHeader(OCP_APIM_SUBSCRIPTION_KEY,
                    ConfigHandler.getBingCustomSearchAPIsubscriptionKey());

            org.apache.http.HttpResponse response = httpclient.execute(request);
            HttpEntity entity = response.getEntity();
            searchResponse.complete(
                    new SearchResponse(EntityUtils.toString(entity), inputText));
            System.out.println(EntityUtils.toString(entity));
        }
        catch (URISyntaxException| IOException | NullPointerException e) {
            searchResponse.complete(
                    new SearchResponse("", inputText));
        }
    }



    public static void main(String[] args) {
        ConfigHandler.loadConfig();
        new BingSearchAPI().startBingSearch("test").getSnippetListFuture().thenAccept(string -> {
            System.out.println(string);
        });
    }
}
