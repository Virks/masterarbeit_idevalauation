package api.microsoftSearch.data;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ExecptionHandler;
import util.StopWordsHelper;

import java.util.*;

public class SearchResponse {
    private String inputText;
    private String responseString;

    public SearchResponse(String responseString, String inputText) {
        this.responseString = responseString;
        this.inputText = inputText;
    }

    public int getTotalEstimatedMatches() {
        int totalEstimatedMatches = 0;
        try {
            JsonObject searchResponseJson = JsonParser.parseString(responseString).getAsJsonObject();
            JsonElement webPages = searchResponseJson.get("webPages");
            if (webPages.isJsonObject()) {
                totalEstimatedMatches = Integer.parseInt(webPages.getAsJsonObject().get("totalEstimatedMatches").toString());
            }
        }   catch (Exception e) {
            e.printStackTrace();
        }
        return totalEstimatedMatches;
    }

    public List<String> getSnippetsFromSeach() {
        List<String> snippetList = new ArrayList<>();
        try {
            JsonObject searchResponseJson = JsonParser.parseString(responseString).getAsJsonObject();
            JsonElement webPages = searchResponseJson.get("webPages");
            if (webPages.isJsonObject()) {
                JsonElement value = webPages.getAsJsonObject().get("value");
                if (value.isJsonArray()) {
                    value.getAsJsonArray().forEach(valueElement -> {
                        if (valueElement.isJsonObject()) {
                            String snippet = valueElement.getAsJsonObject().get("snippet").getAsString();
                            snippetList.add(snippet);
                        }
                    });
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return snippetList;
    }

    public Map<String,Integer> getTopThreeMostUsedWordsInSnippets() {
        Map<String,Integer> topWords = new HashMap<>();
        try {
            JsonObject searchResponseJson = JsonParser.parseString(responseString).getAsJsonObject();
            JsonElement webPages = searchResponseJson.get("webPages");
            if (webPages.isJsonObject()) {
                JsonElement value = webPages.getAsJsonObject().get("value");
                if (value.isJsonArray()) {
                    value.getAsJsonArray().forEach(valueElement -> {
                        if (valueElement.isJsonObject()) {
                            String[] words = valueElement.getAsJsonObject().get("snippet").toString().split("\\s+");
                            for (int i = 0; i < words.length; i++) {
                                words[i] = words[i].replaceAll("[^\\w]", "");
                                if (!StopWordsHelper.isStopWordOrPlural(inputText, words[i])) {
                                    topWords.merge(words[i], 1, Integer::sum);
                                }
                            }
                        }
                    });
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return sortByValue(topWords);
    }

    private Map<String, Integer> sortByValue(Map<String, Integer> map) {
        List<Map.Entry<String, Integer>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());
        Map<String, Integer> result = new LinkedHashMap<>();
        for (int i = list.size()-1; i >= 0; i--) {
            result.put(list.get(i).getKey(), list.get(i).getValue());
        }
        return result;
    }
}
