package api.twitter;

import configuraton.ConfigHandler;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TwitterAPI {
    Twitter twitter;
    public TwitterAPI() throws TwitterException {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(ConfigHandler.getTwitterOAuthConsumerKey())
                .setOAuthConsumerSecret(ConfigHandler.getTwitterOAuthConsumerSecret())
                .setOAuthAccessToken(ConfigHandler.getTwitterOAuthAccessToken())
                .setOAuthAccessTokenSecret(ConfigHandler.getTwitterOAuthAccessTokenSecret() );
        TwitterFactory tf = new TwitterFactory(cb.build());
        twitter = tf.getInstance();
    }


    public String getSearchResults(String searchQuery, int maxResults) throws TwitterException {
        Query query = new Query(searchQuery);
        QueryResult result = twitter.search(query);
        int count = 0;
        while(result.hasNext()) {
            List<Status> tweets = result.getTweets();
            count += tweets.size();

            if(count > maxResults) {
                break;
            }
        }
        return String.valueOf(count);


//        return result.getTweets().size() > 0 ? result.getTweets().get(0).getText() : "";
    }

    public ArrayList<Status> getReplies(String screenName, long tweetID) {
        ArrayList<Status> replies = new ArrayList<>();

        try {
            Query query = new Query("to:" + screenName + " since_id:" + tweetID);
            QueryResult results;

            do {
                results = twitter.search(query);
                List<Status> tweets = results.getTweets();
                for (Status tweet : tweets)
                    if (tweet.getInReplyToStatusId() == tweetID)
                        replies.add(tweet);
            } while ((query = results.nextQuery()) != null && replies.size() < 1);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return replies;
    }
    public int scrapeReplyCount(Status status) {
        String url = "https://twitter.com/" + status.getUser().getScreenName() + "/status/" + status.getId();
        Document doc = null;
        try {
            doc = Jsoup.connect(url).get();
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        Elements elements = doc.getElementsByClass("permalink-tweet-container");
        if(elements != null && elements.size() > 0) {
            elements = elements.first().getElementsByClass("ProfileTweet-action--reply");
            if(elements != null && elements.size() > 0) {
                elements = elements.first().getElementsByClass("ProfileTweet-action--reply");
                if (elements != null && elements.size() > 0) {
                    elements = elements.first().getElementsByClass("ProfileTweet-actionCount");
                    if (elements != null && elements.size() > 0) {
                        String replyCount = elements.first().attr("data-tweet-stat-count");
                        if (replyCount != null && !replyCount.isEmpty()) {
                            return Integer.parseInt(replyCount);
                        }
                    }
                }
            }
        }

        return 0;
    }

    public static void main(String[] args) throws TwitterException {
        ConfigHandler.loadConfig();
        String test = new TwitterAPI().getSearchResults("test", ConfigHandler.getTwitterMaxSearchResults());
        System.out.println(test);
    }
}
