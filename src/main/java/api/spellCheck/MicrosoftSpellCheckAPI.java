package api.spellCheck;

import api.spellCheck.spellCheckData.FlaggedToken;
import api.spellCheck.spellCheckData.Suggestion;
import com.google.gson.*;
import configuraton.ConfigHandler;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import util.LoggingFuture;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MicrosoftSpellCheckAPI {
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String APPLICATION_X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";
    public static final String POST = "POST";
    public static final String OCP_APIM_SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";
    private String inputText;
//    private LoggingFuture<List<FlaggedToken>> responseFuture = new LoggingFuture<>();

    public LoggingFuture<List<FlaggedToken>> getResponseFuture() {
        return responseFuture;
    }

    public MicrosoftSpellCheckAPI requestTextAnalysisAPI(String inputText) {
        this.inputText = inputText;
        new Thread(() -> {
            try {
                URIBuilder builder = new URIBuilder(ConfigHandler.getMicrosoftSpellCheckAPIHost() + ConfigHandler.getMicrosoftSpellCheckAPIPath());
                builder.setParameter("mkt", ConfigHandler.getMicrosoftSpellCheckAPILanguage());
                builder.setParameter("mode", ConfigHandler.getMicrosoftSpellCheckAPIMode());
                URL url = new URL(builder.build().toString());
                HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                connection.setRequestMethod(POST);
                connection.setRequestProperty(CONTENT_TYPE, APPLICATION_X_WWW_FORM_URLENCODED);
                connection.setRequestProperty(OCP_APIM_SUBSCRIPTION_KEY, ConfigHandler.getMicrosoftSpellCheckAPISubscriptionKey());
                //noinspection DuplicatedCode
                connection.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.writeBytes("text=" + inputText);
                wr.flush();
                wr.close();
                StringBuilder response = new StringBuilder();
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String line;
                while ((line = in.readLine()) != null) {
                    response.append(line);
                }
                in.close();

                JsonArray flaggedTokensArray = readFlaggedTokensFromJson(response.toString());
                responseFuture.complete(readSuggestionsFromFlaggedToken(flaggedTokensArray));
            } catch (URISyntaxException | IOException e) {
                responseFuture.complete(new ArrayList<>());
                e.printStackTrace();
            }
        }).start();
        return this;
    }

    private LoggingFuture<List<FlaggedToken>> responseFuture
            = new LoggingFuture<>();

    public void requestTextAnalysisAPI() {
        try(CloseableHttpClient httpclient = HttpClients.createDefault()) {
            String spellcheckEndpoint
                    = ConfigHandler.getMicrosoftSpellCheckAPIHost()
                    + ConfigHandler.getMicrosoftSpellCheckAPIPath();
            URIBuilder builder = new URIBuilder(spellcheckEndpoint);

            builder.setParameter("text", "Bill Gatas");
            builder.setParameter("mode",
                    ConfigHandler.getMicrosoftSpellCheckAPIMode());
            builder.setParameter("mkt",
                    ConfigHandler.getMicrosoftSpellCheckAPILanguage());

            HttpGet request = new HttpGet(builder.build());
            request.setHeader(OCP_APIM_SUBSCRIPTION_KEY,
                    ConfigHandler.getMicrosoftSpellCheckAPISubscriptionKey());

            HttpResponse response = httpclient.execute(request);;

            JsonArray flaggedTokensArray
                    = readFlaggedTokensFromJson(response.toString());
            responseFuture.complete(
                    readSuggestionsFromFlaggedToken(flaggedTokensArray));
        } catch (URISyntaxException| IOException | NullPointerException e) {
            responseFuture.complete(new ArrayList<>());
        }
    }

    private JsonArray readFlaggedTokensFromJson(String body) {
        try {
            if (body != null && !body.isEmpty()) {
                JsonElement bodyElement = JsonParser.parseString(body);
                if (bodyElement.isJsonObject()) {
                    JsonObject searchResponseJson = bodyElement.getAsJsonObject();
                    JsonElement documentElements = searchResponseJson.get("flaggedTokens");
                    if (documentElements.isJsonArray()) {
                        return documentElements.getAsJsonArray();
                    }
                }
            }
        }catch (JsonSyntaxException e) {
            System.out.println("text Analysis API didnt return a JSON document: " + body);
        }
        return new JsonArray();
    }

    private List<FlaggedToken> readSuggestionsFromFlaggedToken(JsonArray flaggedTokensArray) {
        List<FlaggedToken> flaggedTokens = new ArrayList<>();
        flaggedTokensArray.forEach( flaggedToken -> {
            if(flaggedToken.isJsonObject()) {
                JsonObject flaggedTokenObject = flaggedToken.getAsJsonObject();
                int offset = flaggedTokenObject.has("offset") ? flaggedTokenObject.get("offset").getAsInt() : 0;
                String token = flaggedTokenObject.has("token") ? flaggedTokenObject.get("token").getAsString() : "";
                String type = flaggedTokenObject.has("type") ? flaggedTokenObject.get("type").getAsString() : "";
                List<Suggestion> suggestions = new ArrayList<>();
                if(flaggedTokenObject.has("suggestions") && flaggedTokenObject.get("suggestions").isJsonArray()) {
                    flaggedTokenObject.get("suggestions").getAsJsonArray().forEach(suggenstionElement -> {
                        if(suggenstionElement.isJsonObject()) {
                            JsonObject suggestionObject = suggenstionElement.getAsJsonObject();
                            String suggestion = suggestionObject.get("suggestion").getAsString();
                            double score = suggestionObject.get("score").getAsDouble();
                            suggestions.add(new Suggestion(suggestion,score));
                        }
                    });
                    flaggedTokens.add(new FlaggedToken(offset,token,type,suggestions));
                }
            }
        });
        return flaggedTokens;
    }

    public String getInputText() {
        return this.inputText;
    }

    public static void main(String[] args) throws IOException, URISyntaxException {

        ConfigHandler.loadConfig();
        new MicrosoftSpellCheckAPI().requestTextAnalysisAPI();
    }
}
