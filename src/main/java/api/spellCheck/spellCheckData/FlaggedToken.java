package api.spellCheck.spellCheckData;

import java.util.List;

public class FlaggedToken {
    private Integer offset;
    private String token;
    private String type;
    private List<Suggestion> suggestionList;

    public FlaggedToken(Integer offset, String token, String type, List<Suggestion> suggestionList) {
        this.offset = offset;
        this.token = token;
        this.type = type;
        this.suggestionList = suggestionList;
    }

    public Integer getOffset() {
        return offset;
    }

    public String getToken() {
        return token;
    }

    public String getType() {
        return type;
    }

    public List<Suggestion> getSuggestionList() {
        return suggestionList;
    }

    @Override
    public String toString() {
        return "FlaggedToken{" +
                "offset=" + offset +
                ", token='" + token + '\'' +
                ", type='" + type + '\'' +
                ", suggestionList=" + suggestionList +
                '}';
    }
}
