package api.spellCheck.spellCheckData;

public class Suggestion {
    private String suggestion;
    private double score;

    public Suggestion(String suggestion, double score) {
        this.suggestion = suggestion;
        this.score = score;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Suggestion{" +
                "suggestion='" + suggestion + '\'' +
                ", score=" + score +
                '}';
    }
}
