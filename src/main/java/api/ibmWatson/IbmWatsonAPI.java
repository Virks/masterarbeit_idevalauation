package api.ibmWatson;
import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.natural_language_understanding.v1.NaturalLanguageUnderstanding;
import com.ibm.watson.natural_language_understanding.v1.model.*;
import configuraton.ConfigHandler;
import util.IdeatorLogger;
import util.LoggingFuture;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;


public class IbmWatsonAPI {
    private String requestText;
    private LoggingFuture<SentimentResult> sentimentFuture = new LoggingFuture<>();
    private LoggingFuture<List<CategoriesResult>> categoriesFuture = new LoggingFuture<>();
    private LoggingFuture<List<KeywordsResult>> keywordsFuture = new LoggingFuture<>();
//    private LoggingFuture<List<CategoriesResult>> categoriesFuture = new LoggingFuture<>();
////    private CompletableFuture<List<ConceptsResult>> conceptsFuture = new CompletableFuture<>();
//    private LoggingFuture<List<KeywordsResult>> keywordsFuture = new LoggingFuture<>();
//    private LoggingFuture<SentimentResult> sentimentFuture = new LoggingFuture<>();
//    private CompletableFuture<List<EmotionResult>> emotionFuture = new CompletableFuture<>();

    public IbmWatsonAPI() {
    }
    public IbmWatsonAPI startKeyWordsAndSentimentSearch(CompletableFuture<String> requestTextFuture) {
        requestTextFuture.thenAccept(requestText -> {
            if (requestText.length() > 14999) {
                requestText = requestText.substring(0,14999);
            }
            this.requestText = requestText;
            new Thread(this::requestKeywordsAndSentiment).start();
        });
        return this;
    }
    public IbmWatsonAPI startKeyWordsAndSentimentSearch(String requestText) {
        if (requestText.length() > 14999) {
            requestText = requestText.substring(0,14999);
        }
        this.requestText = requestText;
        requestKeywordsAndSentiment();
        return this;
    }

    public IbmWatsonAPI startKeywordsSearch(CompletableFuture<String> requestTextFuture) {
        requestTextFuture.thenAccept(requestText -> {
            if (requestText.length() > 14999) {
                requestText = requestText.substring(0, 14999);
            }
            this.requestText = requestText;
            requestKeywords();
        });
        return this;
    }

    public IbmWatsonAPI startSentimentSearch(CompletableFuture<String> requestTextFuture) {
        requestTextFuture.thenAccept(requestText -> {
            if (requestText.length() > 14999) {
                requestText = requestText.substring(0, 14999);
            }
            this.requestText = requestText;
            requestsSentiment();
        });
        return this;
    }

    public IbmWatsonAPI startCategoriesAndKeywords(String requestText) {
        if (requestText.length() > 14999) {
            requestText = requestText.substring(0,14999);
        }
        this.requestText = requestText;
        requestCategoriesAndKeywords();
        return this;
    }

    private void requestsSentiment() {
        NaturalLanguageUnderstanding service = getNLUService();
        SentimentOptions sentimentOptions = new SentimentOptions.Builder()
                .build();
        Features features = new Features.Builder()
                .sentiment(sentimentOptions)
                .build();
        AnalysisResults results = getAnalysisResults(service, features);
        sentimentFuture.complete(results.getSentiment());
    }

    private AnalysisResults getAnalysisResults(
            NaturalLanguageUnderstanding service,
            Features features) {
        AnalyzeOptions parameters = new AnalyzeOptions.Builder()
                .text(requestText)
                .features(features)
                .build();
        return service.analyze(parameters)
                .execute()
                .getResult();
    }

    private void requestCategories() {
        NaturalLanguageUnderstanding service = getNLUService();
        CategoriesOptions categoriesOptions = new CategoriesOptions.Builder()
                .explanation(true)
                .build();
        Features features = new Features.Builder()
                .categories(categoriesOptions)
                .build();
        AnalysisResults results = getAnalysisResults(service, features);
        categoriesFuture.complete(results.getCategories());
    }

    private void requestKeywords() {
        NaturalLanguageUnderstanding service = getNLUService();
        KeywordsOptions keywordsOptions = new KeywordsOptions.Builder()
                .build();
        Features features = new Features.Builder()
                .keywords(keywordsOptions)
                .build();
        AnalysisResults results = getAnalysisResults(service, features);
        keywordsFuture.complete(results.getKeywords());
    }

    /**
     * for input text
     */
    private void requestCategoriesAndKeywords() {
            NaturalLanguageUnderstanding service = getNLUService();
            CategoriesOptions categoriesOptions = new CategoriesOptions.Builder().explanation(true).limit(15).build();
            KeywordsOptions keywordsOptions = new KeywordsOptions.Builder().build();
            Features features = new Features.Builder()
                    .categories(categoriesOptions)
                    .keywords(keywordsOptions)
                    .build();
            finishRequest(service, features);
    }

    /**
     * meant for comments
     */
    private void requestKeywordsAndSentiment() {
            NaturalLanguageUnderstanding service = getNLUService();
            KeywordsOptions keywordsOptions = new KeywordsOptions.Builder().sentiment(true).emotion(true).build();
            SentimentOptions sentimentOptions = new SentimentOptions.Builder().build();
            Features features = new Features.Builder()
                    .sentiment(sentimentOptions)
                    .keywords(keywordsOptions)
                    .build();
            finishRequest(service, features);
    }

    private void finishRequest(NaturalLanguageUnderstanding service, Features features) {
        AnalysisResults results = getAnalysisResults(service, features);
        categoriesFuture.complete(results.getCategories());
        sentimentFuture.complete(results.getSentiment());
        keywordsFuture.complete(results.getKeywords());
    }

    public void startRequestAll() {
        new Thread(this::requestKeywordsAndSentiment).start();
    }

    private void requestAll() {
        try {
            NaturalLanguageUnderstanding service = getNLUService();
            CategoriesOptions categoriesOptions = new CategoriesOptions.Builder().explanation(true).build();
//            ConceptsOptions conceptsOptions = new ConceptsOptions.Builder().build();
            EmotionOptions emotionOptions = new EmotionOptions.Builder()./* keywords from topic maybe?.targets() */build();
            KeywordsOptions keywordsOptions = new KeywordsOptions.Builder().sentiment(true).emotion(true).build();
            SentimentOptions sentimentOptions = new SentimentOptions.Builder()./* keywords from topic maybe?.targets() */build();
            Features features = new Features.Builder()
                    .categories(categoriesOptions)
                    .sentiment(sentimentOptions)
                    .keywords(keywordsOptions)
//                    .concepts(conceptsOptions)
//                    .emotion(emotionOptions)
                    .build();
            finishRequest(service, features);
        } catch(Exception e) {
            e.printStackTrace();
            finishExceptionally();
        }
    }

    private NaturalLanguageUnderstanding getNLUService() {
        Authenticator authenticator = new IamAuthenticator(ConfigHandler.getIBMAuthKey());
        NaturalLanguageUnderstanding service = new NaturalLanguageUnderstanding("2019-07-12", authenticator);
        service.setServiceUrl("https://gateway-lon.watsonplatform.net/natural-language-understanding/api");
        return service;
    }

    public static String transformKeywordsToString(List<KeywordsResult> keywordsResults) {
        StringBuilder keywordString = new StringBuilder("");
        //TODO check why this can be null
        int counter = 0;
        if(keywordsResults != null && !keywordsResults.isEmpty()) {
            for(int i = 0; counter < 5 && i < keywordsResults.size(); i++) {
                if (keywordsResults.get(i) != null && keywordsResults.get(i).getText() != null) {
                    String keyword = keywordsResults.get(i).getText();
                        keywordString.append(keyword).append(" ");
                        counter++;
                }
            }
        }
        return keywordString.toString();
    }

    public static String transformAllKeywordsToString(List<KeywordsResult> keywordsResults) {
        StringBuilder keywordString = new StringBuilder("");
        //TODO check why this can be null
        if(keywordsResults != null && !keywordsResults.isEmpty()) {
            for(int i = 0; i < keywordsResults.size(); i++) {
                if (keywordsResults.get(i) != null && keywordsResults.get(i).getText() != null) {
                    String keyword = keywordsResults.get(i).getText();
                    keywordString.append(keyword).append(", ");
                }
            }
        }
        return keywordString.toString();
    }

    public static List<KeywordsResult> removeEntrysWithEigennamen(List<KeywordsResult> listWithEigennamen) {
        for(int i = 0; i < listWithEigennamen.size(); ++i) {
            if(listWithEigennamen.get(i).getText().contains("StayHome")
                    || listWithEigennamen.get(i).getText().contains("CoroNow")
                    || listWithEigennamen.get(i).getText().contains("JoinIn")
                    || listWithEigennamen.get(i).getText().contains("deinLaufbursche")) {
                listWithEigennamen.remove(i);
            }
        }
        return listWithEigennamen;
    }

    public static String transformStringListToString(List<String> keywordsResults) {
        StringBuilder keywordString = new StringBuilder("");
        //TODO check why this can be null
        if(keywordsResults != null && !keywordsResults.isEmpty()) {
            for(int i = 0; i < keywordsResults.size(); i++) {
                if (keywordsResults.get(i) != null) {
                    keywordString.append(keywordsResults.get(i)).append(" ");
                }
            }
        }
        return keywordString.toString();
    }


    private void finishExceptionally() {
        IdeatorLogger.debug("Exception with Watson api");
        categoriesFuture.complete(new ArrayList<>());
        sentimentFuture.complete(new SentimentResult());
        keywordsFuture.complete(new ArrayList<>());
    }

    public LoggingFuture<List<CategoriesResult>> getCategoriesFuture() {
        return categoriesFuture;
    }

//    public CompletableFuture<List<ConceptsResult>> getConceptsFuture() {
//        return conceptsFuture;
//    }

    public LoggingFuture<List<KeywordsResult>> getKeywordsFuture() {
        return keywordsFuture;
    }

    public LoggingFuture<SentimentResult> getSentimentFuture() {
        return sentimentFuture;
    }

    public String getRequestText() {
        return this.requestText.replaceAll("[^a-zA-Z ]", "");
    }


}
